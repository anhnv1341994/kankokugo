//
//  MenuViewController.swift
//  KankokuGo
//
//  Created by Nguyen Anh on 09/04/2021.
//  Copyright © 2021 tld. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /**
     *  Array to display menu options
     */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        tblMenuOptions.delegate = self
        tblMenuOptions.dataSource = self
        tblMenuOptions.register(cellType: MenuTableViewCell.self)

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        arrayMenuOptions = [["title":"Trang chủ", "icon":"ic_home"],
                            ["title":"Chia sẻ", "icon":"ic_share"],
                            ["title":"Facebook", "icon":"ic_facebook"],
                            ["title":"Youtube", "icon":"ic_youtube"],
//                            ["title": Defaults[.endDate] != nil ? "Đăng ký gói tháng (Hết hạn: \(Defaults[.endDate]!.toString())" : "Đăng ký gói tháng" , "icon":"purchase"],
        ]
        tblMenuOptions.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        navigationController?.setNavigationBarHidden(false, animated: true)
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
//
//        cell.selectionStyle = UITableViewCell.SelectionStyle.none
//        cell.layoutMargins = UIEdgeInsets.zero
//        cell.preservesSuperviewLayoutMargins = false
//        cell.backgroundColor = UIColor.clear
//
//        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
//        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
//
//        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"] ?? "")
//        lblTitle.text = arrayMenuOptions[indexPath.row]["title"] ?? ""
//
//        return cell
        
        let cell = tableView.dequeueReusableCell(with: MenuTableViewCell.self, for: indexPath)
        cell.lbTitle.text = arrayMenuOptions[indexPath.row]["title"] ?? ""
        cell.imgView.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"] ?? "")
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}
