//
//  TracNghiemViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 05/16/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import WebKit

class Question {
    var number: String?
    var question: String?
    var point: Int = 1
    var answer1: Answer?
    var answer2: Answer?
    var answer3: Answer?
    var answer4: Answer?
    var answer: String?
}

class Answer {
    var check: Bool = false
    var answer: String?
    init(check: Bool, answer: String) {
        self.check = check
        self.answer = answer
    }
}

class TracNghiemViewController: BaseViewController, CheckAnswer {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var viewAds: UIView!
    @IBOutlet weak var height_ads: NSLayoutConstraint!
    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var btnNopbai: UIButton!
    private var enterAnimationFinished = false
    private var lastVisibleItem : IndexPath?
    var content: String = ""
    var list_question: [Question] = []
    var time_default = 0
    var timer = Timer()
    var view_ansewer = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.heightWebView.constant = 0
        self.tableView.register(UINib(nibName: "ItemTracNghiemTableViewCell", bundle: nil), forCellReuseIdentifier: "tracNghiemCell")
        self.content = self.content.replacingOccurrences(of: "<li>", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "</ol>", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "\r", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "\n", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "\t", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "width=\"360\"", with: "width=\"\(Int(self.webView.frame.width*2))px\"", options: .literal, range: nil)
            .replacingOccurrences(of: "height=\"55\"", with: "height=\"100px\"", options: .literal, range: nil)
        self.configWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.checkExpiredOffAds() {
            self.height_ads.constant = Constant.height_ads
            DispatchQueue.main.async {
                self.showAds(self.viewAds)
            }
        }else {
            self.height_ads.constant = 0
            self.removeAds()
        }
        //        self.animateView()
    }
    
    func loadWebView(html: String) {
        webView.loadHTMLString(html, baseURL: nil)
    }
    
    @objc func countDownTime() {
        self.time_default -= 1
        self.navigationItem.title = "\(self.time_default)s"
        if self.time_default == 0 {
            self.timer.invalidate()
            self.navigationItem.title = "HẾT GIỜ"
        }
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownTime), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        self.timer.invalidate()
    }
    
    func getListAnswer() {
        var time = ""
        var question_split = ""
        if content.count > 1 {
            if  self.content.range(of: "%") != nil {
                if self.content.components(separatedBy: CharacterSet(charactersIn: "%")).count > 0 {
                    let split_content = self.content.components(separatedBy: CharacterSet(charactersIn: "%"))
                    time = split_content[0].trimmingCharacters(in: .whitespaces).components(separatedBy: "s")[0]
                    self.tableView.tag = Int(time) ?? 0
                    self.time_default = Int(time) ?? 0
                    self.startTimer()
                    question_split = split_content[1]
                }else {
                    question_split = self.content
                }
            }
            
            if question_split.range(of: "$") != nil {
                let video_split = question_split.components(separatedBy: CharacterSet(charactersIn: "$"))
                let video = video_split[0].trimmingCharacters(in: .whitespaces)
                self.heightWebView.constant = 100
                DispatchQueue.main.async {
                    self.loadWebView(html: video)
                }
                question_split = video_split[1].trimmingCharacters(in: .whitespaces)
            }
            
            if question_split.range(of: "@") != nil {
                let questions = question_split.components(separatedBy: CharacterSet(charactersIn: "@"))
                if questions.count > 0 {
                    var cauhoi: Int = 1
                    for question in questions {
                        if question.trimmingCharacters(in: .whitespaces).count > 0 {
                            let _question = question.components(separatedBy: "<ol>")
                            let title_question = _question[0].trimmingCharacters(in: .whitespaces)
                            
                            let answers = _question[1].components(separatedBy: "</li>")
                            let obj_question = Question()
                            if title_question.range(of: "(+") != nil {
                                let point = title_question.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                                    .components(separatedBy: "+")[1]
                                    .components(separatedBy: " ")[0]
                                if Double(point) != nil {
                                    obj_question.point = Int(point) ?? 1
                                }
                            }
                            obj_question.number = "Câu \(cauhoi):"
                            obj_question.question = title_question
                            obj_question.answer1 = Answer(check: false, answer: answers[0].trimmingCharacters(in: .whitespaces))
                            obj_question.answer2 = Answer(check: false, answer: answers[1].trimmingCharacters(in: .whitespaces))
                            obj_question.answer3 = Answer(check: false, answer: answers[2].trimmingCharacters(in: .whitespaces))
                            obj_question.answer4 = Answer(check: false, answer: answers[3].trimmingCharacters(in: .whitespaces))
                            obj_question.answer = answers[4].components(separatedBy: "Đáp án:")[1].trimmingCharacters(in: .whitespaces)
                            let answerTrue = obj_question.answer ?? ""
                            if obj_question.answer1?.answer == answerTrue {
                                obj_question.answer = "A. \(obj_question.answer!)"
                            }else if obj_question.answer2?.answer == answerTrue {
                                obj_question.answer = "B. \(obj_question.answer!)"
                            }else if obj_question.answer3?.answer == answerTrue {
                                obj_question.answer = "C. \(obj_question.answer!)"
                            }else if obj_question.answer4?.answer == answerTrue {
                                obj_question.answer = "D. \(obj_question.answer!)"
                            }
                            self.list_question.append(obj_question)
                            cauhoi += 1
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func check(indexRow: Int, answer: Int) { // kiểm tra khi chọn đáp án
        let cell = self.tableView.cellForRow(at: IndexPath(item: indexRow, section: 0)) as! ItemTracNghiemTableViewCell
        let item = self.list_question[indexRow]
        cell.imgAnsweA.image = UIImage(named: "radio_uncheck")
        cell.imgAnsweB.image = UIImage(named: "radio_uncheck")
        cell.imgAnsweC.image = UIImage(named: "radio_uncheck")
        cell.imgAnsweD.image = UIImage(named: "radio_uncheck")
        let answerUnCheck1 = Answer(check: false, answer: item.answer1?.answer ?? "")
        let answerUnCheck2 = Answer(check: false, answer: item.answer2?.answer ?? "")
        let answerUnCheck3 = Answer(check: false, answer: item.answer3?.answer ?? "")
        let answerUnCheck4 = Answer(check: false, answer: item.answer4?.answer ?? "")
        item.answer1 = answerUnCheck1
        item.answer2 = answerUnCheck2
        item.answer3 = answerUnCheck3
        item.answer4 = answerUnCheck4
        
        switch answer {
        case 1:
            cell.imgAnsweA.animateView()
            cell.imgAnsweA.image = UIImage(named: "radio_check")
            let answerCheck = Answer(check: true, answer: item.answer1?.answer ?? "")
            item.answer1 = answerCheck
        case 2:
            cell.imgAnsweB.animateView()
            cell.imgAnsweB.image = UIImage(named: "radio_check")
            let answerCheck = Answer(check: true, answer: item.answer2?.answer ?? "")
            item.answer2 = answerCheck
        case 3:
            cell.imgAnsweC.animateView()
            cell.imgAnsweC.image = UIImage(named: "radio_check")
            let answerCheck = Answer(check: true, answer: item.answer3?.answer ?? "")
            item.answer3 = answerCheck
        case 4:
            cell.imgAnsweD.animateView()
            cell.imgAnsweD.image = UIImage(named: "radio_check")
            let answerCheck = Answer(check: true, answer: item.answer4?.answer ?? "")
            item.answer4 = answerCheck
        default:
            break
        }
    }
    
    func checkAnswerTrue() -> Int {
        var count = 0
        for answer in self.list_question {
            let answerTrue = answer.answer ?? ""
            if answer.answer1!.check {
                if let answer1 = answer.answer1?.answer, answer1 == answerTrue {
                    count += 1
                }
            }else if answer.answer2!.check {
                if let answer2 = answer.answer2?.answer, answer2 == answerTrue {
                    count += 1
                }
            }else if answer.answer3!.check {
                if let answer3 = answer.answer3?.answer, answer3 == answerTrue {
                    count += 1
                }
            }else if answer.answer4!.check {
                if let answer4 = answer.answer4?.answer, answer4 == answerTrue {
                    count += 1
                }
            }
        }
        return count
    }
    
    func setAnsewerMakeAgain() {
        for answer in self.list_question {
            let answer1 = answer.answer1
            answer1?.check = false
            answer.answer1 = answer1
            
            let answer2 = answer.answer2
            answer2?.check = false
            answer.answer2 = answer2
            
            let answer3 = answer.answer3
            answer3?.check = false
            answer.answer3 = answer3
            
            let answer4 = answer.answer4
            answer4?.check = false
            answer.answer4 = answer4
        }
        self.time_default = self.tableView.tag
        self.btnNopbai.alpha = 1
        self.view_ansewer = false
        self.startTimer()
        self.tableView.reloadData()
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        sender.animateView()
        self.stopTimer()
        self.showAlerYesNo(title: "Thông báo", "Ok", "Cancel", "Bạn muốn kết thúc bài thi!", yesHandler: { (alertYes) in
            self.btnNopbai.alpha = 0
            self.showAlerYesNo(title: "Kết quả", "Xem đáp án", "Làm lại", "Điểm đạt \(self.checkAnswerTrue())/\(self.list_question.count)\nThời gian dư: \(self.time_default)s", yesHandler: { (alertYes) in
                self.navigationItem.title = "ĐÁP ÁN"
                self.view_ansewer = true
                self.tableView.reloadData()
            }) { (alertNo) in
                self.setAnsewerMakeAgain()
            }
        }) { (alertNo) in
            self.startTimer()
        }
    }
}

extension TracNghiemViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list_question.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tracNghiemCell") as! ItemTracNghiemTableViewCell
        cell.delegate = self
        cell.index = indexPath.item
        let item = self.list_question[indexPath.item]
        cell.txtNumberQuestion.text = item.number
        cell.txtQuestion.text = item.question
        cell.imgAnsweA.image = UIImage(named: "radio_uncheck")
        cell.imgAnsweB.image = UIImage(named: "radio_uncheck")
        cell.imgAnsweC.image = UIImage(named: "radio_uncheck")
        cell.imgAnsweD.image = UIImage(named: "radio_uncheck")
        
        cell.txtAnsweA.text = item.answer1?.answer ?? ""
        if item.answer1!.check {
            cell.imgAnsweA.image = UIImage(named: "radio_check")
        }
        cell.txtAnsweB.text = item.answer2?.answer ?? ""
        if item.answer2!.check {
            cell.imgAnsweB.image = UIImage(named: "radio_check")
        }
        cell.txtAnsweC.text = item.answer3?.answer ?? ""
        if item.answer3!.check {
            cell.imgAnsweC.image = UIImage(named: "radio_check")
        }
        cell.txtAnsweD.text = item.answer4?.answer ?? ""
        if item.answer4!.check {
            cell.imgAnsweD.image = UIImage(named: "radio_check")
        }
        if self.view_ansewer {
            //            cell.marginLeftLabel.constant = 0
            cell.isUserInteractionEnabled = false
            cell.viewAnswer.alpha = 1
        }else {
            //            cell.marginLeftLabel.constant = 24
            cell.isUserInteractionEnabled = true
            cell.viewAnswer.alpha = 0
        }
        //        cell.imgAnsweA.isHidden = self.view_ansewer
        //        cell.imgAnsweB.isHidden = self.view_ansewer
        //        cell.imgAnsweC.isHidden = self.view_ansewer
        //        cell.imgAnsweD.isHidden = self.view_ansewer
        return cell
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if self.view_ansewer {
//            return 274
//        }else {
//            return 250
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if self.view_ansewer {
        //            return 274
        //        }else {
        //            return 250
        //        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.lastVisibleItem == nil  {
            self.lastVisibleItem=tableView.indexPathsForVisibleRows?.last
        }
        guard let lastVisibleRow = lastVisibleItem?.row , !enterAnimationFinished else {
            return
        }
        if indexPath.row <= lastVisibleRow {
            let delay:TimeInterval = Double(indexPath.row) * 0.15
            cell.animateView(delay: delay)
        }
        if indexPath.row >= lastVisibleRow{
            self.enterAnimationFinished = true
        }
    }
}

extension TracNghiemViewController: UIScrollViewDelegate,  WKUIDelegate, WKNavigationDelegate {
    func configWebView() {
        webView.scrollView.bounces = false
        //        if #available(iOS 11.0, *) {
        //            webView.scrollView.contentInsetAdjustmentBehavior = .never
        //        } else {
        //            self.automaticallyAdjustsScrollViewInsets = false
        //        }
        webView.contentMode = .center
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        self.getListAnswer()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url), let url = self.webView.url, UIApplication.shared.canOpenURL(url) {
            print("### URL:", url)
            
        }
        
        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            // When page load finishes. Should work on each page reload.
            if self.webView.estimatedProgress == 1 {
                print("### EP:", self.webView.estimatedProgress)
                
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
        if let link = webView.url?.absoluteString {
            print("Did commit url: \(link)")
        } else {
            print("Did commit")
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        // Allow navigation for requests loading external web content resources.
        guard navigationAction.targetFrame?.isMainFrame != false else {
            decisionHandler(.allow)
            return
        }
        return
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print("start provisional")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("fail to load")
        
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("fail provision")
        
    }
    
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print("Process terminate")
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let protectSpace = challenge.protectionSpace
        let sender = challenge.sender
        
        if protectSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let truct = challenge.protectionSpace.serverTrust {
                let credential = URLCredential(trust: truct)
                let Device = UIDevice.current
                let iosVersion = NSString(string: Device.systemVersion).doubleValue
                if iosVersion >= 9.0 && iosVersion < 10.0 {
                    sender?.use(credential, for: challenge)
                }
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
            }
            
        } else {
            let credential = URLCredential(user: "", password: "", persistence: .forSession)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
        }
    }
}
