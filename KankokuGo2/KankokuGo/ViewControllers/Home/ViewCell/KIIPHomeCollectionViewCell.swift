//
//  KIIPHomeCollectionViewCell.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 13/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class KIIPHomeCollectionViewCell: FSPagerViewCell {
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(_ named: String?) {
        self.imgView.contentMode = .scaleToFill
        self.imgView.image = UIImage(named: named ?? "")
    }
}
