//
//  AlertRegisterPayViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 06/01/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

class AlertRegisterPayViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionExit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionRegisterMonth(_ sender: Any) {
        RageProducts.store.buyProductWithProductIdentifier(RageProducts.identifierConsumable)    }
}
