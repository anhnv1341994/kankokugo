//
//  HomeViewController.swift
//  KankokuGo
//
//  Created by tld on 3/27/19.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import MarqueeLabel

enum Key: String {
    case topik, topik1, topik2, kiip, grammar, translate, multiple_choice, other, detailLuyenviet,
    nguphapsocap, nguphaptrungcap, nguphaptopik1, nguphaptopik2, baiviettopikiithamkhao, maucauviettopik2
    , tracnghiemtopik1, tracnghiemtopik2,tracnghiemnguphap , tracnghiemtuvungchude, tracnghiemtuvunghanhan
    , tuvungtheohanhan,tuvungtheochude
    , tuvungtrungcap, tuvungsocap
    , luyenviettopik2
    , duhoc, dulich, sinhhoat
    , huongdankiip
}

class HomeViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewAds: UIView!
    @IBOutlet weak var height_ads: NSLayoutConstraint!
    @IBOutlet weak var lbMessage: MarqueeLabel!
    var arrayDataClass = [Dictionary<String,String>]()
    let column: CGFloat = 2
    let minItemSpacingIphone: CGFloat = 20.0
    let minItemSpacingIpad: CGFloat = 30.0
    let leftItemSpacingIphone: CGFloat = 20.0
    let leftItemSpacingIpad: CGFloat = 30.0
    var totalWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = nil
        self.collectionView.register(viewType: KIIPBannerCollectionReusableView.self, forElementKind: UICollectionView.elementKindSectionHeader)
        setupDataClass()
        self.checkDateExpiredInAppPurchase()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.checkExpiredOffAds() {
//            self.startAnimation()
            self.height_ads.constant = Constant.height_ads
            DispatchQueue.main.async {
                self.showAds(self.viewAds)
            }
        }else {
            self.lbMessage.text = nil
            self.height_ads.constant = 0
            self.removeAds()
        }
    }
    
    func startAnimation() {
        // Continuous, with tap to pause
        lbMessage.isUserInteractionEnabled = true
        lbMessage.tag = 501
        lbMessage.type = .continuous
        lbMessage.speed = .duration(20)
        lbMessage.fadeLength = 10.0
        lbMessage.trailingBuffer = 30.0
        lbMessage.text = "Đăng ký gói tháng (5,900 Won/ 2 tháng). Lợi ích: Xóa bỏ hoàn toàn quảng cáo trên App, làm trắc nghiệm và xem bài giảng Video !"
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        lbMessage.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        RageProducts.store.buyProductWithProductIdentifier(RageProducts.identifierConsumable)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupDataClass() {
        arrayDataClass = [["name":"TOPIK", "key": Key.topik.rawValue, "hour": "0h"],
                          ["name":"KIIP", "key": Key.kiip.rawValue, "hour": "0h"],
                          ["name":"NGỮ PHÁP", "key": Key.grammar.rawValue, "hour": "0h"],
                          ["name":"TỪ VỰNG", "key": Key.translate.rawValue, "hour": "0h"],
                          ["name":"MUA SẮM", "key": Key.multiple_choice.rawValue, "hour": "20h"],
                          ["name":"KHÁC", "key": Key.other.rawValue, "hour": "50h"]]
        collectionView.reloadData()
    }
    
    private func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        if UIDevice.isPad() {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIpad) - (leftItemSpacingIpad * 2)
        } else {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIphone) - (leftItemSpacingIphone * 2)
        }
        return totalWidth / column
    }
    
    private func getMarginTop(itemWidth: CGFloat, boundWidth: CGFloat) -> CGFloat {
        var marginTopBottom: CGFloat = 20.0
        let count_item = CGFloat(self.arrayDataClass.count / Int(self.column))
        if UIDevice.isPad() {
            let height_all_item = (itemWidth * count_item + ((count_item - 1) * minItemSpacingIpad))
            if height_all_item >= boundWidth {
                let height = height_all_item - boundWidth
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        } else {
            let height_all_item = (itemWidth * count_item + ((count_item - 1) * minItemSpacingIphone))
            if height_all_item <= boundWidth {
                let height = boundWidth - height_all_item
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        }
    }
    
    func getDataDanhmuc(_ term_id: String) {
        NetworkManager.sharedManager.getDataDetail(term_id) { (response, error) in
            if let data = response?.data, data.count > 0 {
                let webVC = KIIPWebViewController()
                webVC.txtTitle = data.first?.post_title
                webVC.link = data.first?.post_content ?? ""
                webVC.isNotSubPoint = true
                self.pushViewWithScreen(webVC)
            }
        }
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableCell(with: KIIPBannerCollectionReusableView.self, elementKind: kind, for: indexPath)
        view.bannerView.didSelectItemAt = { [weak self] id in
            guard let id = id, let self = self else { return }
            self.getDataDanhmuc(id)
        }
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: self.view.frame.height * 160.0 / 896.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayDataClass.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(ofType: ClassCollectionCell.self, for: indexPath) {
            cell.nameLabel?.text = arrayDataClass[indexPath.row]["name"] ?? ""
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.animateView(delay: Double(indexPath.item) * 0.1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = arrayDataClass[indexPath.row]
        let name = arrayDataClass[indexPath.row]["name"] ?? ""
        let key = arrayDataClass[indexPath.row]["key"] ?? ""
        switch item["key"] {
        case Key.topik.rawValue:
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKViewController.self)
            topik.title = name
            self.navigationController?.pushViewController(topik, animated: true)
        case Key.kiip.rawValue:
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: KIIPViewController.self)
            topik.title = name
            self.navigationController?.pushViewController(topik, animated: true)
        case Key.grammar.rawValue:
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKIViewController.self)
            topik.title = name
            topik.topickVC = key
            self.navigationController?.pushViewController(topik, animated: true)
        case Key.translate.rawValue:
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKIViewController.self)
            topik.title = name
            topik.topickVC = key
            self.navigationController?.pushViewController(topik, animated: true)
        case Key.multiple_choice.rawValue:
            let vc = KIIPADSViewController()
            self.pushViewWithScreen(vc)
//            let url_fb = "https://itunes.apple.com/app/id1458634673"
//            if url_fb != "" {
//                self.openURL(url_fb, "")
//            }
        case Key.other.rawValue:
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKIViewController.self)
            topik.title = name
            topik.topickVC = key
            self.navigationController?.pushViewController(topik, animated: true)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if UIDevice.isPad() {
            return UIEdgeInsets(top: 15, left: leftItemSpacingIpad, bottom: 15, right: leftItemSpacingIpad)
        } else {
            return UIEdgeInsets(top: 15, left: leftItemSpacingIphone, bottom: 15, right: leftItemSpacingIphone)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.isPad() {
            return minItemSpacingIpad
        } else {
            return minItemSpacingIphone
        }
    }
    
}

class MNNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let themeColor: UIColor = UIColor().navigationColor()
        UINavigationBar.appearance().barTintColor = themeColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        UIBarButtonItem.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().backgroundColor = UIColor().navigationColor()
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = UIColor.white
        UITabBar.appearance().barTintColor = UIColor().navigationColor()
    }
}
