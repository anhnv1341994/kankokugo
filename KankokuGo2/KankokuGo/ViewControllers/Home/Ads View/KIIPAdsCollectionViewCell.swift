//
//  KIIPAdsCollectionViewCell.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 28/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class KIIPAdsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: VAImageView!
    @IBOutlet weak var lbMonth: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
