//
//  MNConstan.swift
//  SupportCustomer
//
//  Created by Nguyen Van Anh on 3/7/19.
//  Copyright © 2019 Nguyen Van Anh. All rights reserved.
//

import UIKit

class Constant: NSObject {
    static let baseURL = "http://apphanquoclythu.com/api/lumen-core/public/v1"
    static let api_solop = "http://apphanquoclythu.com/api/lumen-core/public/v1/terms/"
    static let api_danhmuc = "http://apphanquoclythu.com/api/lumen-core/public/v1/posts/term/"
    static let api_search = "http://apphanquoclythu.com/api/lumen-core/public/v1/posts/term/"
    static let api_banner = "http://apphanquoclythu.com/api/lumen-core/public/v1/posts/"
    static let api_adsID = "\(Constant.baseURL)/terms/quangcao"
    static let api_adsList = "\(Constant.baseURL)/posts/term/get-ad/%@/%@/"

    
    static let url_facebook = "https://www.facebook.com/hanquoclythu2018/"
    static let app_facebook = "fb://profile/209225342869778"
    static let url_app = "http://itunes.apple.com/app/id1458634673"
//    static let url_youtube = "https://www.youtube.com/channel/UCTuYuujpNk00z0qFAEvkXPQ"
    static let url_youtube = "youtube://www.youtube.com/channel/UCTuYuujpNk00z0qFAEvkXPQ"
    
//    static let ads_banner = "ca-app-pub-3940256099942544/2934735716" //test
//    static let ads_full = "ca-app-pub-3940256099942544/4411468910" // test
//    static let ads_add_xu = "ca-app-pub-3940256099942544/1712485313" //test
    
        static let ads_banner = "ca-app-pub-8709952403611439/4879576415" //that
        static let ads_full = "ca-app-pub-8709952403611439/3553697083" //that
        static let ads_add_xu = "ca-app-pub-8709952403611439/9720839556" // that
        static let ads_id = "ca-app-pub-8709952403611439~3118582818" // that
    
    static let point = "point"
    static let point_ads = "point_ads"
    static let start_date = "start_date"
    static let end_date = "end_date"
    static let enable = "enable"
    static let format_dmyhms = "format_dmyhms"
    static let format_dmy = "format_dmy"
    static let ymdhms: String = "yyyy-MM-dd HH:mm:ss"
    static let dmyhms: String = "dd-MM-yyyy HH:mm:ss"
    static let ymd: String = "yyyy-MM-dd"
    static let dmy: String = "dd-MM-yyyy"
    
    static let width_border: CGFloat = 0.7
    static let corner_radius: CGFloat = 6
    static let height_ads: CGFloat = 85
}
