//
//  ContainerWebView.swift
//  Hydrobank
//
//  Created by MIchael San MInay on 04/10/2019.
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit
import WebKit

class ContainerWebView: CustomViewXib {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

    lazy var webView: WKWebView = {
        let webView = WKWebView()
        webView.isOpaque = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.configuration.suppressesIncrementalRendering = true
        return webView
    }()
    var didFinish: (() -> Void)?

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        if let _ = self.superview {
            self.containerView.addSubview(self.webView)

            let constraints = [
                self.webView.topAnchor.constraint(equalTo: self.topAnchor),
                self.webView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                self.webView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                self.webView.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ]

            constraints.forEach {
                $0.isActive = true
            }
        }
    }

    func loadUrl(_ url: String, timeoutInterval: TimeInterval = 60) {
        guard let url = URL(string: url) else {
            return
        }

        self.showLoadingView(true)

        let urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeoutInterval)
        self.webView.load(urlRequest)
        self.webView.navigationDelegate = self
    }

    func loadHTML(htmlContent: String) {
        webView.loadHTMLString(htmlContent, baseURL: nil)
        self.webView.navigationDelegate = self
    }

    func showLoadingView(_ isShow: Bool) {
        if isShow {
            self.loadingIndicator.startAnimating()
        }
        else {
            self.loadingIndicator.stopAnimating()
        }
    }
}

extension ContainerWebView: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        self.showLoadingView(false)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            if let url = navigationAction.request.url, UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                decisionHandler(.cancel)
            } else {
                decisionHandler(.allow)
            }
        } else {
            decisionHandler(.allow)
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.didFinish?()
        self.showLoadingView(false)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.showLoadingView(false)
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.showLoadingView(false)
    }

}

