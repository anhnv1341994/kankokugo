//
//  WKWebView+.swift
//  ShuwaTaishi
//
//  Created by tld on 10/8/18.
//  Copyright © 2018 tld. All rights reserved.
//

import UIKit
import WebKit

extension WKWebView {
    
    func loadWithHeader(_ request: URLRequest) {
        var request = request
        request.setValue("Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1", forHTTPHeaderField: "User-Agent")
        self.load(request)
    }
    
}
