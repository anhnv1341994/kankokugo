//
//  KiemXuHocBaiView.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/15/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

protocol HandleViewVideo: class {
    func handleViewADS()
}

class KiemXuHocBaiView: UITableViewCell {
    @IBOutlet weak var txtXu: UILabel!
    @IBOutlet weak var viewKiemXu: UIView!
    var handleDelegate: HandleViewVideo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func handleViewADS(_ sender: UITapGestureRecognizer) {
        if self.handleDelegate != nil {
            self.handleDelegate?.handleViewADS()
        }
    }
}
