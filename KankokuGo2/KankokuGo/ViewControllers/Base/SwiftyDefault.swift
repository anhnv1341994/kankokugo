//
//  SwiftyDefault.swift
//  SupportCustomer
//
//  Created by Nguyen Van Anh on 3/7/19.
//  Copyright © 2019 Nguyen Van Anh. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

extension DefaultsKeys {
    static let point = DefaultsKey<Int?>(Constant.point)
    static let point_ads = DefaultsKey<Int?>(Constant.point_ads)
    static let startDate = DefaultsKey<Date?>(Constant.start_date)
    static let endDate = DefaultsKey<Date?>(Constant.end_date)
    static let enable = DefaultsKey<Bool?>(Constant.enable)
}
