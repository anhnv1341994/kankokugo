//
//  MNNetworkManager.swift
//  SupportCustomer
//
//  Created by Nguyen Van Anh on 3/7/19.
//  Copyright © 2019 Nguyen Van Anh. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireJsonToObjects
import EVReflection

class NetworkState {
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class NetworkManager: SessionManager {
    static let sharedManager: NetworkManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 15
        return NetworkManager(configuration: configuration)
    }()
    
    func get_data_term_post (_ str: String, completion:@escaping (_ response: DataResponse<GetDataResponse>) -> Void) {
        let params: [String: Any] = [:]
        let url = URL(string: Constant.api_danhmuc + str)
        _ = self.requestJsonMethod(url: url!, params: params, completion: completion)
    }
    
    func get_data_term (_ str: String, completion:@escaping (_ response: DataResponse<GetDataResponse>) -> Void) {
        let params: [String: Any] = ["page": 1,
                                     "per_page": 20]
        let url = URL(string: Constant.api_solop + str)
        _ = self.requestJsonMethod(url: url!, params: params, completion: completion)
    }
    
    func searchData (_ term_id: NSNumber, title: String, completion:@escaping (_ response: DataResponse<GetDataResponse>) -> Void) {
        let params: [String: Any] = [:]
        var components = URLComponents()
        components.scheme = "http"
        components.host = "apphanquoclythu.com"
        components.path = "/api/lumen-core/public/v1/posts/term/\(term_id)/serch"
        components.queryItems = [
            URLQueryItem(name: "title", value: title),
        ]
        _ = self.requestJsonMethod(url: components.url!, params: params, completion: completion)
    }
    
    @discardableResult func getDataDetail(_ id: String, completion: @escaping (BannerResponse?, Error?) -> Void) -> DataRequest? {
        let url = Constant.api_banner + id
        return self.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, ofType: BannerResponse.self) { (result, error) in
            completion(result, error)
        }
    }
    
    @discardableResult func getDataAdsID(completion: @escaping (TermsResponse?, Error?) -> Void) -> DataRequest? {
        let url = Constant.api_adsID
        return self.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, ofType: TermsResponse.self) { (result, error) in
            completion(result, error)
        }
    }

    @discardableResult func getDataAdsList(_ id: String, pageNumber: String, completion: @escaping (AdsResponse?, Error?) -> Void) -> DataRequest? {
        let url = String(format: Constant.api_adsList, id, pageNumber)
        return self.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, ofType: AdsResponse.self) { (result, error) in
            completion(result, error)
        }
    }
    
    func requestJsonMethod<T:BaseResponse>(url: URL, params: [String : Any]?, completion: @escaping (_ result: DataResponse<T>) -> Void) -> Request {
        let urlString = URLRequest(url: url).url!.absoluteString
        return Alamofire.request(urlString, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseObject(completionHandler: { (response:DataResponse<T>) in
            switch response.result{
            case .success( _):
                guard response.error == nil else {
                    print(response.error!)
                    return
                }
                guard response.data != nil else {
                    print("Data is empty")
                    return
                }
                if (params?["image"]) != nil {
                    
                }else {
                    print("\n*************************************************")
                    print("\nLink API : ", urlString)
                    print("Params: ", params! as Any)
                    print("\n")
                    print("*************************************************\n")
                }
                completion(response)
            case .failure(let error):
                print("Hình như lỗi server: ", error)
                completion(response)
            }
        })
    }
    
    @discardableResult func request<T: Codable>(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding, ofType: T.Type, completion: @escaping (T?, Error?) -> Void) -> DataRequest {
        print("PARAMS - ", parameters ?? [])
        print("URL - ", url)
        let request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: nil).response { (response) in
            if let data = response.data {
                do {
                    let item = try JSONDecoder().decode(T.self, from: data)
                    completion(item, nil)
                } catch {
                    completion(nil, error)
                }
            } else if let error = response.error {
                completion(nil, error)
            }
        }
        return request
    }
    
    func nilString(_ value: String?) -> String{
        if let v = value {
            return v
        }
        return ""
    }
    
    func nilNumber(_ value: NSNumber?) -> NSNumber{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilInt(_ value: Int?) -> Int{
        if let n = value {
            return n
        }
        return 0
    }
    
    func formatStringDate (_ date_str: String, _ format: String) -> String{
        if date_str != "" {
            if format == Constant.format_dmyhms {
                let split_date = date_str.components(separatedBy: " ")
                let date_day = split_date[0].components(separatedBy: "-")
                let hour = split_date[1]
                return "\(date_day[2])-\(date_day[1])-\(date_day[0]) \(hour)"
            }else if format == Constant.format_dmy {
                let date_day = date_str.components(separatedBy: "-")
                return "\(date_day[2])-\(date_day[1])-\(date_day[0])"
            }
        }
        return ""
    }
}


