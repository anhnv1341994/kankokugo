//
//  AppDelegate.swift
//  KankokuGo
//
//  Created by tld on 3/27/19.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import GoogleMobileAds
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
     let notificationCenter = UNUserNotificationCenter.current()
    var window: UIWindow?


     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
          FirebaseApp.configure()

     notificationCenter.delegate = self
     
     let options: UNAuthorizationOptions = [.alert, .sound, .badge]
     
     notificationCenter.requestAuthorization(options: options) {
          (didAllow, error) in
          if !didAllow {
               print("User has declined notifications")
          }
     }
     
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor.black
          navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
     window = UIWindow(frame: UIScreen.main.bounds)
     window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
     window?.makeKeyAndVisible()
     
//     DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
//          self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
//     }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
     UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate: UNUserNotificationCenterDelegate {
     
     func userNotificationCenter(_ center: UNUserNotificationCenter,
                                 willPresent notification: UNNotification,
                                 withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
          
          completionHandler([.alert, .sound])
     }
     
     func userNotificationCenter(_ center: UNUserNotificationCenter,
                                 didReceive response: UNNotificationResponse,
                                 withCompletionHandler completionHandler: @escaping () -> Void) {
          
          if response.notification.request.identifier == "Local Notification" {
               print("Handling notifications with the Local Notification Identifier")
               RageProducts.store.buyProductWithProductIdentifier(RageProducts.identifierConsumable)
          }
          
          completionHandler()
     }
     
     func scheduleNotification(message: String) {
          
          let content = UNMutableNotificationContent() // Содержимое уведомления
          let categoryIdentifire = "Notification"
          
          content.title = "Gia hạn gói"
          content.body = message
          content.sound = UNNotificationSound.default
          content.badge = 1
          content.categoryIdentifier = categoryIdentifire
          
          let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
          let identifier = "Local Notification"
          let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
          
          notificationCenter.add(request) { (error) in
               if let error = error {
                    print("Error \(error.localizedDescription)")
               }
          }
          
          //          let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
          //          let deleteAction = UNNotificationAction(identifier: "DeleteAction", title: "Delete", options: [.destructive])
          let category = UNNotificationCategory(identifier: categoryIdentifire,
                                                actions: [],
                                                intentIdentifiers: [],
                                                options: [])
          
          notificationCenter.setNotificationCategories([category])
     }
}

extension UICollectionView {
    func register<T: UICollectionReusableView>(viewType: T.Type, forElementKind elementKind: String, bundle: Bundle? = nil) {
        let nib = UINib(nibName: String(describing: T.self), bundle: bundle)
        self.register(nib, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: T.self.description())
    }

    func dequeueReusableCell<T: UICollectionReusableView>(with type: T.Type, elementKind: String, for indexPath: IndexPath) -> T {
        return self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: T.self.description(), for: indexPath) as! T
    }
}
