//
//  RageProducts.swift
//  CalmApp
//
//  Created by steadfast-MO23 on 6/11/18.
//  Copyright © 2018 steadfast. All rights reserved.
//

import Foundation

public struct RageProducts {
    
    public static let identifierConsumable = "com.kankokugo2018.kankokugo2.NonRenewingSubscription"
//    public static let identifierNonRenew = "com.kankokugo2018.kankokugo.NonRenewingSubscription"
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [RageProducts.identifierConsumable]
    
    public static let store = IAPHelper(productIds: RageProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
