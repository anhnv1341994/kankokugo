//
//  AccountModel.swift
//  SupportCustomer
//
//  Created by Nguyen Van Anh on 3/7/19.
//  Copyright © 2019 Nguyen Van Anh. All rights reserved.
//

import UIKit
import EVReflection


class BaseResponse: EVObject {
    var jsonrpc: String?
    var code: NSNumber?
    var error: String?
    var message: String?
    var id: NSNumber?
}

class GetDataResponse: BaseResponse {
    var result: String?
    var data: GetData?
}

class GetData: BaseResponse {
    var terms: Terms?
    var posts: Posts?
}

class Terms: BaseResponse {
    var current_page: NSNumber?
    var data: [DataTerm]?
    var first_page_url: String?
    var from: String?
    var last_page: NSNumber?
    var last_page_url: String?
    var next_page_url: String?
    var path: String?
    var per_page: NSNumber?
    var prev_page_url: String?
    var to: NSNumber?
    var total: NSNumber?
}

class DataTerm: BaseResponse {
    var term_id: NSNumber?
    var name: String?
    var slug: String?
    var term_group: NSNumber?
}

class Posts: BaseResponse {
    var current_page: NSNumber?
    var data: [DataPosts]?
    var first_page_url: String?
    var from: String?
    var last_page: NSNumber?
    var last_page_url: String?
    var next_page_url: String?
    var path: String?
    var per_page: NSNumber?
    var prev_page_url: String?
    var to: NSNumber?
    var total: NSNumber?
}

class DataPosts: BaseResponse {
    var ID: NSNumber?
    var post_title: String?
    var post_content: String?
    var post_date_gmt: String?
    var post_date: String?
    var post_author: NSNumber?
}


struct AdsResponse: Codable {
    let data: [AdsData]?
}



struct TermsResponse: Codable {
    var code: Int?
    var data: TermsData1?
}

struct TermsData1: Codable {
    var terms: Terms1?
}

struct Terms1: Codable {
    var current_page: Int?
    var data: [Term]?
    var first_page_url: String?
    var from: Int?
    var last_page: Int?
    var last_page_url: String?
    var next_page_url: String?
    var path: String?
    var per_page: Int?
    var prev_page_url: String?
    var to: Int?
    var total: Int?
}

struct Term: Codable {
    var term_id: String?
    var name: String?
    var slug: String?
    var term_group: String?
}


// MARK: - Welcome
struct AdsData: Codable {
    let id: Int
    let date, dateGmt: String
    let guid: GUID
    let modified, modifiedGmt, slug, status: String
    let type: String
    let link: String
    let title: GUID
    let content, excerpt: Content
    let author, featuredMedia: Int
    let commentStatus, pingStatus: String
    let sticky: Bool
    let template, format: String
    let meta: [JSONAny]
    let categories: [Int]
    let tags: [JSONAny]
    let links: WelcomeLinks
    let embedded: Embedded

    enum CodingKeys: String, CodingKey {
        case id, date
        case dateGmt = "date_gmt"
        case guid, modified
        case modifiedGmt = "modified_gmt"
        case slug, status, type, link, title, content, excerpt, author
        case featuredMedia = "featured_media"
        case commentStatus = "comment_status"
        case pingStatus = "ping_status"
        case sticky, template, format, meta, categories, tags
        case links = "_links"
        case embedded = "_embedded"
    }
}

// MARK: - Content
struct Content: Codable {
    let rendered: String
    let protected: Bool
}

// MARK: - Embedded
struct Embedded: Codable {
    let author: [EmbeddedAuthor]
    let wpFeaturedmedia: [WpFeaturedmedia]
    let wpTerm: [[EmbeddedWpTerm]]

    enum CodingKeys: String, CodingKey {
        case author
        case wpFeaturedmedia = "wp:featuredmedia"
        case wpTerm = "wp:term"
    }
}

// MARK: - EmbeddedAuthor
struct EmbeddedAuthor: Codable {
    let id: Int
    let name, url, authorDescription: String
    let link: String
    let slug: String
    let avatarUrls: [String: String]
    let links: AuthorLinks

    enum CodingKeys: String, CodingKey {
        case id, name, url
        case authorDescription = "description"
        case link, slug
        case avatarUrls = "avatar_urls"
        case links = "_links"
    }
}

// MARK: - AuthorLinks
struct AuthorLinks: Codable {
    let linksSelf, collection: [About]

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection
    }
}

// MARK: - About
struct About: Codable {
    let href: String
}

// MARK: - WpFeaturedmedia
struct WpFeaturedmedia: Codable {
    let id: Int
    let date, slug, type: String
    let link: String
    let title: GUID
    let author: Int
    let caption: GUID
    let altText, mediaType: String
    let mimeType: MIMEType
    let mediaDetails: MediaDetails
    let sourceURL: String
    let links: WpFeaturedmediaLinks

    enum CodingKeys: String, CodingKey {
        case id, date, slug, type, link, title, author, caption
        case altText = "alt_text"
        case mediaType = "media_type"
        case mimeType = "mime_type"
        case mediaDetails = "media_details"
        case sourceURL = "source_url"
        case links = "_links"
    }
}

// MARK: - GUID
struct GUID: Codable {
    let rendered: String
}

// MARK: - WpFeaturedmediaLinks
struct WpFeaturedmediaLinks: Codable {
    let linksSelf, collection, about: [About]
    let author, replies: [ReplyElement]

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection, about, author, replies
    }
}

// MARK: - ReplyElement
struct ReplyElement: Codable {
    let embeddable: Bool
    let href: String
}

// MARK: - MediaDetails
struct MediaDetails: Codable {
    let width, height: Int
    let file: String
    let sizes: [String: Size]
    let imageMeta: ImageMeta

    enum CodingKeys: String, CodingKey {
        case width, height, file, sizes
        case imageMeta = "image_meta"
    }
}

// MARK: - ImageMeta
struct ImageMeta: Codable {
    let aperture, credit, camera, caption: String
    let createdTimestamp, copyright, focalLength, iso: String
    let shutterSpeed, title, orientation: String
    let keywords: [JSONAny]

    enum CodingKeys: String, CodingKey {
        case aperture, credit, camera, caption
        case createdTimestamp = "created_timestamp"
        case copyright
        case focalLength = "focal_length"
        case iso
        case shutterSpeed = "shutter_speed"
        case title, orientation, keywords
    }
}

// MARK: - Size
struct Size: Codable {
    let file: String
    let width, height: Int
    let mimeType: MIMEType
    let sourceURL: String

    enum CodingKeys: String, CodingKey {
        case file, width, height
        case mimeType = "mime_type"
        case sourceURL = "source_url"
    }
}

enum MIMEType: String, Codable {
    case imageJPEG = "image/jpeg"
}

// MARK: - EmbeddedWpTerm
struct EmbeddedWpTerm: Codable {
    let id: Int
    let link: String
    let name, slug, taxonomy: String
    let links: WpTermLinks

    enum CodingKeys: String, CodingKey {
        case id, link, name, slug, taxonomy
        case links = "_links"
    }
}

// MARK: - WpTermLinks
struct WpTermLinks: Codable {
    let linksSelf, collection, about, wpPostType: [About]
    let curies: [Cury]

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection, about
        case wpPostType = "wp:post_type"
        case curies
    }
}

// MARK: - Cury
struct Cury: Codable {
    let name, href: String
    let templated: Bool
}

// MARK: - WelcomeLinks
struct WelcomeLinks: Codable {
    let linksSelf, collection, about: [About]
    let author, replies: [ReplyElement]
    let versionHistory: [VersionHistory]
    let predecessorVersion: [PredecessorVersion]
    let wpFeaturedmedia: [ReplyElement]
    let wpAttachment: [About]
    let wpTerm: [LinksWpTerm]
    let curies: [Cury]

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection, about, author, replies
        case versionHistory = "version-history"
        case predecessorVersion = "predecessor-version"
        case wpFeaturedmedia = "wp:featuredmedia"
        case wpAttachment = "wp:attachment"
        case wpTerm = "wp:term"
        case curies
    }
}

// MARK: - PredecessorVersion
struct PredecessorVersion: Codable {
    let id: Int
    let href: String
}

// MARK: - VersionHistory
struct VersionHistory: Codable {
    let count: Int
    let href: String
}

// MARK: - LinksWpTerm
struct LinksWpTerm: Codable {
    let taxonomy: String
    let embeddable: Bool
    let href: String
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() { }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}
