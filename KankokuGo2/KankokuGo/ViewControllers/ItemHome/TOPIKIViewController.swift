//
//  TOPIKOtherViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/11/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

class TOPIKIViewController: BaseViewController{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewAds: UIView!
    @IBOutlet weak var height_ads: NSLayoutConstraint!
    var arrayDataClass = [Dictionary<String,String>]()
    let column: CGFloat = 2
    let minItemSpacingIphone: CGFloat = 20.0
    let minItemSpacingIpad: CGFloat = 30.0
    let leftItemSpacingIphone: CGFloat = 20.0
    let leftItemSpacingIpad: CGFloat = 30.0
    var totalWidth: CGFloat = 0
    var topickVC: String = Key.topik1.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        addSlideMenuButton()
        switch topickVC {
        case Key.topik1.rawValue:
            setupDataClassTOPICKI()
        case Key.grammar.rawValue:
            setupDataClassGrammar()
        case Key.translate.rawValue:
            setupDataClassTranslate()
        case Key.other.rawValue:
            setupDataClassOther()
        case Key.detailLuyenviet.rawValue:
            setupDataClassDetailLuyenviet()
        default:
            setupDataClassTOPICKII()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        if self.checkExpiredOffAds() {
            self.height_ads.constant = Constant.height_ads
            DispatchQueue.main.async {
                self.showAds(self.viewAds)
            }
        }else {
            self.height_ads.constant = 0
            self.removeAds()
        }
//        self.animateView()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupDataClassTOPICKI() {
        arrayDataClass.append(["name":"NGỮ PHÁP TOPIK I", "slug": Key.nguphaptopik1.rawValue ])
        arrayDataClass.append(["name":"TRẮC NGHIỆM", "slug": Key.tracnghiemtopik1.rawValue])
        collectionView.reloadData()
    }
    
    func setupDataClassTOPICKII() {
        arrayDataClass.append(["name":"NGỮ PHÁP TOPIK II", "slug": Key.nguphaptopik2.rawValue ])
        arrayDataClass.append(["name":"LUYỆN VIẾT TOPIK II", "slug": Key.luyenviettopik2.rawValue])
        arrayDataClass.append(["name":"TRẮC NGHIỆM", "slug": Key.tracnghiemtopik2.rawValue])
        collectionView.reloadData()
    }
    
    func setupDataClassDetailLuyenviet() {
        arrayDataClass.append(["name":"MẪU CÂU", "slug": Key.maucauviettopik2.rawValue ])
        arrayDataClass.append(["name":"BÀI THAM KHẢO", "slug": Key.baiviettopikiithamkhao.rawValue])
        collectionView.reloadData()
    }
    
    func setupDataClassGrammar() {
        arrayDataClass.append(["name":"SƠ CẤP", "slug": Key.nguphapsocap.rawValue ])
        arrayDataClass.append(["name":"TRUNG CẤP", "slug": Key.nguphaptrungcap.rawValue])
        arrayDataClass.append(["name":"TRẮC NGHIỆM", "slug": Key.tracnghiemnguphap.rawValue])
        collectionView.reloadData()
    }
    
    func setupDataClassTranslate() {
        arrayDataClass.append(["name":"TỪ VỰNG THEO CHỦ ĐỀ", "slug": Key.tuvungtheochude.rawValue ])
        arrayDataClass.append(["name":"TỰ VỰNG HÁN HÀN", "slug": Key.tuvungtheohanhan.rawValue])
        arrayDataClass.append(["name":"TRẮC NGHIỆM", "slug": Key.tracnghiemnguphap.rawValue])
        collectionView.reloadData()
    }
    
    func setupDataClassOther() {
        arrayDataClass.append(["name":"DU HỌC", "slug": Key.duhoc.rawValue ])
        arrayDataClass.append(["name":"DU LỊCH", "slug": Key.dulich.rawValue])
        arrayDataClass.append(["name":"SINH HOẠT", "slug": Key.sinhhoat.rawValue])
        collectionView.reloadData()
    }
    
    private func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        if UIDevice.isPad() {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIpad) - (leftItemSpacingIpad * 2)
        } else {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIphone) - (leftItemSpacingIphone * 2)
        }
        return totalWidth / column
    }
    
    private func getMarginTop(itemWidth: CGFloat, boundWidth: CGFloat) -> CGFloat {
        var marginTopBottom: CGFloat = 20.0
        let count_item = CGFloat(self.arrayDataClass.count)
        if UIDevice.isPad() {
            let height_all_item = ((itemWidth/3) * count_item + ((count_item - 1) * minItemSpacingIpad))
            if height_all_item >= boundWidth {
                let height = height_all_item - boundWidth
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        } else {
            let height_all_item = ((itemWidth/3) * count_item + ((count_item - 1) * minItemSpacingIphone))
            if height_all_item <= boundWidth {
                let height = boundWidth - height_all_item
                if height > 40 {
                    marginTopBottom = height / 2
                }
            }
            return marginTopBottom
        }
    }
}

extension TOPIKIViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayDataClass.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(ofType: ClassCollectionCell.self, for: indexPath) {
            cell.nameLabel?.text = arrayDataClass[indexPath.row]["name"] ?? ""
            cell.widthItem.constant = collectionView.bounds.size.width - 40
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        return CGSize(width: collectionView.bounds.size.width, height: itemWidth/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let slug = arrayDataClass[indexPath.row]["slug"] ?? ""
        let name = arrayDataClass[indexPath.row]["name"] ?? ""
        if slug == Key.luyenviettopik2.rawValue {
            let name = arrayDataClass[indexPath.row]["name"] ?? ""
            let topik = self.instantiateViewController(fromStoryboard: .main, ofType: TOPIKIViewController.self)
            topik.title = name
            topik.topickVC = Key.detailLuyenviet.rawValue
            self.navigationController?.pushViewController(topik, animated: true)
        }else {
            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: CategoryListViewController.self)
            webVC.slug = slug
            webVC.nav_title = name
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        let marginTopBottom = self.getMarginTop(itemWidth: itemWidth, boundWidth: collectionView.bounds.size.height)
        if UIDevice.isPad() {
            return UIEdgeInsets(top: marginTopBottom, left: leftItemSpacingIpad, bottom: marginTopBottom, right: leftItemSpacingIpad)
        } else {
            return UIEdgeInsets(top: marginTopBottom, left: leftItemSpacingIphone, bottom: marginTopBottom, right: leftItemSpacingIphone)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.isPad() {
            return minItemSpacingIpad
        } else {
            return minItemSpacingIphone
        }
    }
    
}

