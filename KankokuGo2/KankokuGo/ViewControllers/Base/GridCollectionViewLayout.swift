//
//  GridCollectionViewLayout.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 13/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//


import UIKit

enum GridViewScrollingDirection: Int16 {
    case unknown = 0
    case up
    case down
    case left
    case right
}

protocol GridCollectionViewLayoutDataSource: class {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, lineSpacingForSection section: NSInteger) -> CGFloat
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, interitemSpacingForSection section: NSInteger) -> CGFloat

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, numberItemsPerLineForSection section: NSInteger) -> Int
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, aspectRatioForItemsInSection section: NSInteger) -> CGFloat

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceLengthForHeaderInSection section: NSInteger) -> CGFloat
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceLengthForFooterInSection section: NSInteger) -> CGFloat

}

extension GridCollectionViewLayoutDataSource {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, lineSpacingForSection section: NSInteger) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, interitemSpacingForSection section: NSInteger) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, numberItemsPerLineForSection section: NSInteger) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, aspectRatioForItemsInSection section: NSInteger) -> CGFloat {
        return 1.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceLengthForHeaderInSection section: NSInteger) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceLengthForFooterInSection section: NSInteger) -> CGFloat {
        return 0.0
    }

}

protocol GridCollectionViewReorderDelegate: class {

    func collectionView(_ collectionView: UICollectionView, willBeginMoveItemAtIndexPath indexPath: IndexPath) -> Void
    func collectionView(_ collectionView: UICollectionView, didBeginMoveItemAtIndexPath indexPath: IndexPath) -> Void

    func collectionView(_ collectionView: UICollectionView, willEndMoveItemAtIndexPath indexPath: IndexPath) -> Void
    func collectionView(_ collectionView: UICollectionView, didEndMoveItemAtIndexPath indexPath: IndexPath) -> Void

}

extension GridCollectionViewReorderDelegate {

    func collectionView(_ collectionView: UICollectionView, willBeginMoveItemAtIndexPath indexPath: IndexPath) -> Void {

    }
    func collectionView(_ collectionView: UICollectionView, didBeginMoveItemAtIndexPath indexPath: IndexPath) -> Void {

    }

    func collectionView(_ collectionView: UICollectionView, willEndMoveItemAtIndexPath indexPath: IndexPath) -> Void {

    }
    func collectionView(_ collectionView: UICollectionView, didEndMoveItemAtIndexPath indexPath: IndexPath) -> Void {

    }

}

protocol GridCollectionViewReorderDataSource: class {

    func collectionView(_ collectionView: UICollectionView, canMoveItemAtIndexPath indexPath: IndexPath) -> Bool
    func collectionView(_ collectionView: UICollectionView, canMoveItemAtIndexPath fromIndexPath: IndexPath, toIndexPath: IndexPath) -> Bool
    func collectionView(_ collectionView: UICollectionView, itemAt indexPath: IndexPath, moveTo toIndexPath: IndexPath) -> Void

}

extension GridCollectionViewReorderDataSource {

    func collectionView(_ collectionView: UICollectionView, canMoveItemAtIndexPath indexPath: IndexPath) -> Bool {
        return false
    }

    func collectionView(_ collectionView: UICollectionView, canMoveItemAtIndexPath fromIndexPath: IndexPath, toIndexPath: IndexPath) -> Bool {
        return false
    }

    func collectionView(_ collectionView: UICollectionView, itemAt indexPath: IndexPath, moveTo toIndexPath: IndexPath) -> Void {

    }

}

class GridCollectionViewLayout: UICollectionViewLayout, UIGestureRecognizerDelegate {

    var scrollDirection: UICollectionView.ScrollDirection = UICollectionView.ScrollDirection.vertical {
        didSet {
            self.invalidateLayout()
        }
    }

    var sectionInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            self.invalidateLayout()
        }
    }
    var lineSpacing: CGFloat = 0.0 {
        didSet {
            if oldValue != self.lineSpacing {
                self.invalidateLayout()
            }
        }
    }
    var interitemSpacing: CGFloat = 0.0 {
        didSet {
            if oldValue != self.interitemSpacing {
                self.invalidateLayout()
            }
        }
    }

    var numberOfItemsPerLine: Int = 1 {
        didSet {
            if oldValue != self.numberOfItemsPerLine {
                self.invalidateLayout()
            }
        }
    }
    var aspectRatio: CGFloat = 1.0 {
        didSet {
            if oldValue != self.aspectRatio {
                self.invalidateLayout()
            }
        }
    }

    var headerReferenceLength: CGFloat = 0.0 {
        didSet {
            if oldValue != self.headerReferenceLength {
                self.invalidateLayout()
            }
        }
    }
    var footerReferenceLength: CGFloat = 0.0 {
        didSet {
            if oldValue != self.footerReferenceLength {
                self.invalidateLayout()
            }
        }
    }

    private var collectionViewChanged: NSKeyValueObservation?

    private var headerAttributes = [IndexPath: UICollectionViewLayoutAttributes]()
    private var footerAttributes = [IndexPath: UICollectionViewLayoutAttributes]()
    private var cellAttributesBySection = [[UICollectionViewLayoutAttributes]()]
    private var collectionViewContentLength: CGFloat = 0.0

    weak var layoutDataSource: GridCollectionViewLayoutDataSource?
    weak var reorderDataSource: GridCollectionViewReorderDataSource?
    weak var reorderDelegate: GridCollectionViewReorderDelegate?

    override init() {
        super.init()
        self.listenCollectionViewChanged()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.listenCollectionViewChanged()
    }

    deinit {
        self.collectionViewChanged?.invalidate()
        self.invalidatesScrollTimer()
    }

    override func prepare() {
        self.calculateContentSize()
        self.calculateLayoutAttributes()
    }

    override var collectionViewContentSize: CGSize {
        if self.scrollDirection == .vertical {
            return CGSize.init(width: self.collectionView?.bounds.size.width ?? 0.0, height: self.collectionViewContentLength)
        }
        else {
            return CGSize.init(width: self.collectionViewContentLength, height: self.collectionView?.bounds.size.height ?? 0.0)
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleAttributes = [UICollectionViewLayoutAttributes]()

        for sectionAttributes in self.cellAttributesBySection {
            for layoutAttributes in sectionAttributes {
                if rect.intersects(layoutAttributes.frame) {
                    visibleAttributes.append(layoutAttributes)
                }
            }
        }

        for (_, layoutAttributes) in self.headerAttributes {
            if rect.intersects(layoutAttributes.frame) {
                visibleAttributes.append(layoutAttributes)
            }
        }

        for (_, layoutAttributes) in self.footerAttributes {
            if rect.intersects(layoutAttributes.frame) {
                visibleAttributes.append(layoutAttributes)
            }
        }

        return visibleAttributes
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cellAttributesBySection[indexPath.section][indexPath.item]
    }

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if elementKind == UICollectionView.elementKindSectionHeader {
            return self.headerAttributes[indexPath]
        }
        else if elementKind == UICollectionView.elementKindSectionFooter {
            return self.footerAttributes[indexPath]
        }
        return nil
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return !newBounds.size.equalTo(self.collectionView?.bounds.size ?? CGSize.zero)
    }

    // MARK: - CalculateContentSize

    func calculateContentSize() -> Void {
        var contentLength: CGFloat = 0.0

        let sections = self.collectionView?.numberOfSections ?? 0
        for i in 0..<sections {
            contentLength += self.contentLengthForSection(i)
        }

        self.collectionViewContentLength = contentLength
    }

    func calculateLayoutAttributes() -> Void {
        self.cellAttributesBySection.removeAll()
        self.headerAttributes.removeAll()
        self.footerAttributes.removeAll()

        for section in 0..<(self.collectionView?.numberOfSections ?? 0) {
            let indexPath = IndexPath.init(item: 0, section: section)

            if let attributes = self.headerAttributesForIndexPath(indexPath) {
                self.headerAttributes[indexPath] = attributes
            }

            self.cellAttributesBySection.append(self.layoutAttributesForItemsInSection(section))

            if let attributes = self.footerAttributesForIndexPath(indexPath) {
                self.footerAttributes[indexPath] = attributes
            }
        }

    }

    func sectionInsetForSection(_ section: Int) -> UIEdgeInsets {
        if let collectionView = self.collectionView, let sectionInset = self.layoutDataSource?.collectionView(collectionView, layout: self, insetForSectionAtIndex: section) {
            return sectionInset
        }
        return self.sectionInset
    }

    func lineSpacingForSection(_ section: Int) -> CGFloat {
        if let collectionView = self.collectionView, let lineSpacing = self.layoutDataSource?.collectionView(collectionView, layout: self, lineSpacingForSection: section) {
            return lineSpacing
        }
        return self.lineSpacing
    }

    func interitemSpacingForSection(_ section: Int) -> CGFloat {
        if let collectionView = self.collectionView, let interitemSpacing = self.layoutDataSource?.collectionView(collectionView, layout: self, interitemSpacingForSection: section) {
            return interitemSpacing
        }
        return self.interitemSpacing
    }

    func numberOfItemsPerLineForSection(_ section: Int) -> Int {
        if let collectionView = self.collectionView, let numberOfItemsPerLine = self.layoutDataSource?.collectionView(collectionView, layout: self, numberItemsPerLineForSection: section) {
            return numberOfItemsPerLine
        }
        return self.numberOfItemsPerLine
    }

    func aspectRatioForSection(_ section: Int) -> CGFloat {
        if let collectionView = self.collectionView, let aspectRatio = self.layoutDataSource?.collectionView(collectionView, layout: self, aspectRatioForItemsInSection: section) {
            return aspectRatio
        }
        return self.aspectRatio
    }

    func headerLengthForSection(_ section: Int) -> CGFloat {
        if let collectionView = self.collectionView, let headerReferenceLength = self.layoutDataSource?.collectionView(collectionView, layout: self, referenceLengthForHeaderInSection: section) {
            return headerReferenceLength
        }
        return self.headerReferenceLength
    }

    func footerLengthForSection(_ section: Int) -> CGFloat {
        if let collectionView = self.collectionView, let footerReferenceLength = self.layoutDataSource?.collectionView(collectionView, layout: self, referenceLengthForFooterInSection: section) {
            return footerReferenceLength
        }
        return self.footerReferenceLength
    }

    // MARK: - Reorder

    var scrollingSpeed: CGFloat = 300.0
    var scrollingTriggerEdgeInsets = UIEdgeInsets.zero

    var longPressGestureRecognizer: UILongPressGestureRecognizer? = nil
    var panGestureRecognizer: UIPanGestureRecognizer? = nil

    private var selectedItemIndexPath: IndexPath? = nil
    private var selectedCell: UICollectionViewCell? = nil

    private var currentView: UIView? = nil
    private var currentViewCenter: CGPoint = CGPoint.zero

    private var panTranslationInCollectionView: CGPoint = CGPoint.zero
    private var displayLink: CADisplayLink? = nil

    func listenCollectionViewChanged() {
        self.collectionViewChanged = self.observe(\.collectionView, options: [.new], changeHandler: { (collectionViewFlowLayout, change) in
                if collectionViewFlowLayout.collectionView != nil {
                    self.setupCollectionView()
                }
                else {
                    self.invalidatesScrollTimer()
                    self.removeCollectionView()
                }
            })
    }

    func setupCollectionView() -> Void {
        guard let collectionView = self.collectionView else {
            return
        }

        let longPressGestureRecognizer = UILongPressGestureRecognizer.init(target: self, action: #selector(handleLongPressGesture(_:)))
        longPressGestureRecognizer.delegate = self

        if let gestureRecognizers = self.collectionView?.gestureRecognizers {
            for gesture in gestureRecognizers {
                if let gesture = gesture as? UILongPressGestureRecognizer {
                    gesture.require(toFail: longPressGestureRecognizer)
                }
            }
        }

        let panGestureRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(handlePanGesture(_:)))
        panGestureRecognizer.delegate = self

        collectionView.addGestureRecognizer(longPressGestureRecognizer)
        collectionView.addGestureRecognizer(panGestureRecognizer)

        self.longPressGestureRecognizer = longPressGestureRecognizer
        self.panGestureRecognizer = panGestureRecognizer
    }

    func removeCollectionView() -> Void {
        if let gesture = self.longPressGestureRecognizer {
            gesture.view?.removeGestureRecognizer(gesture)
            gesture.delegate = nil
            self.longPressGestureRecognizer = nil
        }

        if let gesture = self.panGestureRecognizer {
            gesture.view?.removeGestureRecognizer(gesture)
            gesture.delegate = nil
            self.panGestureRecognizer = nil
        }
    }

    func invalidateLayoutIfNecessary() -> Void {
        guard let collectionView = self.collectionView else {
            return
        }

        guard let center = self.currentView?.center, let newIndexPath = self.collectionView?.indexPathForItem(at: center) else {
            return
        }

        guard let previousIndexPath = self.selectedItemIndexPath else {
            return
        }

        if newIndexPath == previousIndexPath {
            return
        }

        guard let canMove = self.reorderDataSource?.collectionView(collectionView, canMoveItemAtIndexPath: previousIndexPath, toIndexPath: newIndexPath), canMove else {
            return
        }

        self.selectedItemIndexPath = newIndexPath
        self.reorderDataSource?.collectionView(collectionView, itemAt: previousIndexPath, moveTo: newIndexPath)

        collectionView.performBatchUpdates({
            collectionView.moveItem(at: previousIndexPath, to: newIndexPath)
        }) { (finished) in
            self.selectedCell?.isHidden = false

            if let cell = collectionView.cellForItem(at: newIndexPath) {
                cell.isHidden = true
                self.selectedCell = cell
            }
        }
    }

    func invalidatesScrollTimer() -> Void {
        if let displayLink = self.displayLink, !displayLink.isPaused {
            displayLink.isPaused = true
            displayLink.invalidate()
        }
        self.displayLink = nil
    }

    func setupScrollTimerInDirection(_ direction: GridViewScrollingDirection) -> Void {
        if let displayLink = self.displayLink, !displayLink.isPaused, displayLink.scrollDirection == direction {
            return
        }

        self.invalidatesScrollTimer()

        let displayLink = CADisplayLink.init(target: self, selector: #selector(handleScroll(_:)))
        displayLink.scrollDirection = direction
        displayLink.add(to: RunLoop.main, forMode: RunLoop.Mode.common)

        self.displayLink = displayLink
    }

    @objc func handleScroll(_ displayLink: CADisplayLink) -> Void {
        if displayLink.scrollDirection == .unknown {
            return
        }

        guard let collectionView = self.collectionView else {
            return
        }

        let frameSize = collectionView.bounds.size
        let contentSize = collectionView.contentSize
        let contentOffset = collectionView.contentOffset
        let contentInset = collectionView.contentInset

        var distance = rint(self.scrollingSpeed * CGFloat(displayLink.duration))
        var translation = CGPoint.zero

        switch displayLink.scrollDirection {
        case .up:
            distance = -distance;
            let minY = 0.0 - contentInset.top

            if (contentOffset.y + distance) <= minY {
                distance = -contentOffset.y - contentInset.top
            }

            translation = CGPoint.init(x: 0.0, y: distance)
            break

        case .down:
            let maxY = max(contentSize.height, frameSize.height) - frameSize.height + contentInset.bottom
            if (contentOffset.y + distance) >= maxY {
                distance = maxY - contentOffset.y
            }

            translation = CGPoint.init(x: 0.0, y: distance)
            break

        case .left:
            distance = -distance
            let minX = 0.0 - contentInset.left

            if (contentOffset.x + distance) <= minX {
                distance = -contentOffset.x - contentInset.left
            }

            translation = CGPoint.init(x: distance, y: 0.0)
            break

        case .right:
            let maxX = max(contentSize.width, frameSize.width) - frameSize.width + contentInset.right

            if (contentOffset.x + distance) >= maxX {
                distance = maxX - contentOffset.x;
            }

            translation = CGPoint.init(x: distance, y: 0.0)
            break

        default:
            break
        }

        self.currentViewCenter = self.currentViewCenter.add(translation)
        self.currentView?.center = self.currentViewCenter.add(self.panTranslationInCollectionView)
        collectionView.contentOffset = contentOffset.add(translation)
    }

    @objc func handleLongPressGesture(_ sender: UILongPressGestureRecognizer) -> Void {
        guard let collectionView = self.collectionView else {
            return
        }

        switch sender.state {
        case .began:
            guard let currentIndexPath = collectionView.indexPathForItem(at: sender.location(in: sender.view)) else {
                return
            }

            guard let canMove = self.reorderDataSource?.collectionView(collectionView, canMoveItemAtIndexPath: currentIndexPath), canMove else {
                return
            }

            guard let cell = collectionView.cellForItem(at: currentIndexPath), let cellRect = self.layoutAttributesForItem(at: currentIndexPath)?.frame else {
                return
            }

            self.selectedItemIndexPath = currentIndexPath
            self.selectedCell = cell

            self.reorderDelegate?.collectionView(collectionView, willBeginMoveItemAtIndexPath: currentIndexPath)

            let reorderView = UIView.init(frame: cellRect)
            reorderView.addSubview(UIImageView.init(image: cell.asImage()))
            collectionView.addSubview(reorderView)

            self.currentView = reorderView
            self.currentViewCenter = reorderView.center

            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.beginFromCurrentState, animations: {
                reorderView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }) { (finished) in
                self.reorderDelegate?.collectionView(collectionView, didBeginMoveItemAtIndexPath: currentIndexPath)
                self.selectedCell?.isHidden = true
            }

            break

        case .cancelled,
             .ended:
            guard let currentIndexPath = self.selectedItemIndexPath else {
                return
            }

            guard let layoutAttributes = self.layoutAttributesForItem(at: currentIndexPath) else {
                return
            }

            self.reorderDelegate?.collectionView(collectionView, willEndMoveItemAtIndexPath: currentIndexPath)

            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.beginFromCurrentState, animations: {
                self.currentView?.center = layoutAttributes.center
                self.currentView?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }) { (finished) in
                self.currentView?.removeFromSuperview()
                self.currentView = nil
                self.currentViewCenter = CGPoint.zero

                if let cell = self.selectedCell {
                    cell.isHidden = false
                }

                self.selectedItemIndexPath = nil
                self.selectedCell = nil

                self.reorderDelegate?.collectionView(collectionView, didEndMoveItemAtIndexPath: currentIndexPath)
            }

            break

        default:
            break
        }
    }

    @objc func handlePanGesture(_ sender: UIPanGestureRecognizer) -> Void {
        guard let collectionView = self.collectionView else {
            return
        }

        switch sender.state {
        case .began,
             .changed:
            self.panTranslationInCollectionView = sender.translation(in: sender.view)

            let center = self.currentViewCenter.add(self.panTranslationInCollectionView)
            self.currentView?.center = center

            self.invalidateLayoutIfNecessary()

            switch self.scrollDirection {
            case .vertical:
                if center.y < collectionView.bounds.minY + self.scrollingTriggerEdgeInsets.top {
                    self.setupScrollTimerInDirection(.up)
                }
                else {
                    if center.y > collectionView.bounds.maxY - self.scrollingTriggerEdgeInsets.bottom {
                        self.setupScrollTimerInDirection(.down)
                    }
                    else {
                        self.invalidatesScrollTimer()
                    }
                }
                break

            case .horizontal:
                if center.x < collectionView.bounds.minX + self.scrollingTriggerEdgeInsets.left {
                    self.setupScrollTimerInDirection(.left)
                }
                else {
                    if center.y > collectionView.bounds.maxX - self.scrollingTriggerEdgeInsets.right {
                        self.setupScrollTimerInDirection(.right)
                    }
                    else {
                        self.invalidatesScrollTimer()
                    }
                }
                break
            @unknown default:
                fatalError()
            }

            break

        case .cancelled,
             .ended:
            self.invalidatesScrollTimer()
            break

        default:
            break
        }
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIControl {
            return false
        }
        return true
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if self.panGestureRecognizer == gestureRecognizer {
            return (self.selectedItemIndexPath != nil)
        }
        return true
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if self.longPressGestureRecognizer == gestureRecognizer {
            return self.panGestureRecognizer == otherGestureRecognizer
        }

        if self.panGestureRecognizer == gestureRecognizer {
            return self.longPressGestureRecognizer == otherGestureRecognizer
        }

        return false
    }

}

extension GridCollectionViewLayout {

    func layoutAttributesForCellAtIndexPath(_ indexPath: IndexPath) -> UICollectionViewLayoutAttributes {
        let attributes = UICollectionViewLayoutAttributes.init(forCellWith: indexPath)
        attributes.frame = self.frameForItemAtIndexPath(indexPath)
        return attributes
    }

    func contentLengthForSection(_ section: Int) -> CGFloat {
        let rowsInSection = CGFloat(self.rowsInSection(section))

        var contentLength = self.lineSpacingForSection(section) * (CGFloat)(rowsInSection - 1)
        contentLength += self.lengthwiseInsetLengthInSection(section)

        let cellSize = self.cellSizeInSection(section)
        if self.scrollDirection == .vertical {
            contentLength += rowsInSection * cellSize.height
        }
        else {
            contentLength += rowsInSection * cellSize.width
        }

        contentLength += self.headerLengthForSection(section)
        contentLength += self.footerLengthForSection(section)

        return contentLength
    }

    func rowsInSection(_ section: Int) -> Int {
        let itemsInSection = self.collectionView?.numberOfItems(inSection: section) ?? 0
        let numberOfItemsPerLine = self.numberOfItemsPerLineForSection(section)
        let rowsInSection = itemsInSection / numberOfItemsPerLine + (itemsInSection % numberOfItemsPerLine > 0 ? 1 : 0)
        return rowsInSection
    }

    func lengthwiseInsetLengthInSection(_ section: Int) -> CGFloat {
        let sectionInset = self.sectionInsetForSection(section)
        if self.scrollDirection == .vertical {
            return sectionInset.top + sectionInset.bottom
        }
        else {
            return sectionInset.left + sectionInset.right
        }
    }

    func frameForItemAtIndexPath(_ indexPath: IndexPath) -> CGRect {
        let cellSize = self.cellSizeInSection(indexPath.section)
        let numberOfItemsPerLine = self.numberOfItemsPerLineForSection(indexPath.section)

        let rowOfItem = CGFloat(indexPath.item / numberOfItemsPerLine)
        let locationInRowOfItem = CGFloat(indexPath.item % numberOfItemsPerLine)

        var frame = CGRect.zero
        frame.size = cellSize

        let sectionStart = self.startOfSection(indexPath.section) + self.headerLengthForSection(indexPath.section)
        let sectionInset = self.sectionInsetForSection(indexPath.section)
        let lineSpacing = self.lineSpacingForSection(indexPath.section)
        let interitemSpacing = self.interitemSpacingForSection(indexPath.section)

        if self.scrollDirection == .vertical {
            frame.origin.x = sectionInset.left + (locationInRowOfItem * cellSize.width) + (interitemSpacing * locationInRowOfItem)
            frame.origin.y = sectionStart + sectionInset.top + (rowOfItem * cellSize.height) + (lineSpacing * rowOfItem)
        }
        else {
            frame.origin.x = sectionStart + sectionInset.left + (rowOfItem * cellSize.width) + (lineSpacing * rowOfItem)
            frame.origin.y = sectionInset.top + (locationInRowOfItem * cellSize.height) + (interitemSpacing * locationInRowOfItem)
        }

        return frame
    }

    func startOfSection(_ section: Int) -> CGFloat {
        var startOfSection: CGFloat = 0.0

        for currentSection in 0..<section {
            startOfSection += self.contentLengthForSection(currentSection)
        }

        return startOfSection
    }

    func cellSizeInSection(_ section: Int) -> CGSize {
        let usableSpace = self.usableSpaceInSection(section)
        let cellLength = usableSpace / CGFloat(self.numberOfItemsPerLineForSection(section))
        let aspectRatio = self.aspectRatioForSection(section)

        if self.scrollDirection == .vertical {
            return CGSize.init(width: cellLength, height: cellLength * (1.0 / aspectRatio))
        }
        else {
            return CGSize.init(width: cellLength * aspectRatio, height: cellLength)
        }
    }

    func usableSpaceInSection(_ section: Int) -> CGFloat {
        let sectionInset = self.sectionInsetForSection(section)
        let interitemSpacing = self.interitemSpacingForSection(section)
        let numberOfItemsPerLine = CGFloat(self.numberOfItemsPerLineForSection(section))

        if self.scrollDirection == .vertical {
            return (self.collectionViewContentSize.width
                    - sectionInset.left
                    - sectionInset.right
                    - ((numberOfItemsPerLine - 1) * interitemSpacing))
        }
        else {
            return (self.collectionViewContentSize.height
                    - sectionInset.top
                    - sectionInset.bottom
                    - ((numberOfItemsPerLine - 1) * interitemSpacing))
        }
    }

    func headerAttributesForIndexPath(_ indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let height = self.headerLengthForSection(indexPath.section)
        if height == 0.0 {
            return nil
        }

        var frame = CGRect.zero
        if self.scrollDirection == .vertical {
            frame.size.width = self.collectionViewContentSize.width
            frame.size.height = height
            frame.origin.x = 0.0
            frame.origin.y = self.startOfSection(indexPath.section)
        }
        else {
            frame.size.width = height
            frame.size.height = self.collectionViewContentSize.height
            frame.origin.x = self.startOfSection(indexPath.section)
            frame.origin.y = 0
        }

        let attributes = UICollectionViewLayoutAttributes.init(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: indexPath)
        attributes.frame = frame

        return attributes
    }

    func footerAttributesForIndexPath(_ indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let height = self.footerLengthForSection(indexPath.section)
        if height == 0.0 {
            return nil
        }

        let sectionStart = self.startOfSection(indexPath.section)
        let sectionLength = self.contentLengthForSection(indexPath.section)
        let footerStart = sectionStart + sectionLength - self.footerLengthForSection(indexPath.section)

        var frame = CGRect.zero
        if self.scrollDirection == .vertical {
            frame.size.width = self.collectionViewContentSize.width
            frame.size.height = height
            frame.origin.x = 0.0
            frame.origin.y = footerStart
        }
        else {
            frame.size.width = height
            frame.size.height = self.collectionViewContentSize.height
            frame.origin.x = footerStart
            frame.origin.y = 0.0
        }

        let attributes = UICollectionViewLayoutAttributes.init(forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, with: indexPath)
        attributes.frame = frame

        return attributes
    }

    func layoutAttributesForItemsInSection(_ section: Int) -> [UICollectionViewLayoutAttributes] {
        var attributes = [UICollectionViewLayoutAttributes]()

        for item in 0..<(self.collectionView?.numberOfItems(inSection: section) ?? 0) {
            attributes.append(self.layoutAttributesForCellAtIndexPath(IndexPath.init(item: item, section: section)))
        }

        return attributes
    }

}

extension CADisplayLink {

    struct Holder {
        static var scrollDirection = GridViewScrollingDirection.unknown
    }

    var scrollDirection: GridViewScrollingDirection {
        get {
            return Holder.scrollDirection
        }
        set(newValue) {
            Holder.scrollDirection = newValue
        }
    }

}

extension CGPoint {

    func add(_ point: CGPoint) -> CGPoint {
        return CGPoint.init(x: self.x + point.x, y: self.y + point.y)
    }

}
extension UIView {
    func asImage() -> UIImage? {
        self.asImage(afterScreenUpdate: true)
    }
    
    func asImage(afterScreenUpdate: Bool) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: afterScreenUpdate)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
