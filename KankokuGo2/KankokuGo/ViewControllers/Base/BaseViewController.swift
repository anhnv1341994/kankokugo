//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import FBSDKShareKit
import JGProgressHUD
import GoogleMobileAds
import SystemConfiguration
import SwiftyUserDefaults

class BaseViewController: UIViewController, SlideMenuDelegate, GADBannerViewDelegate, UISearchBarDelegate, GADRewardBasedVideoAdDelegate, HandleViewVideo {
    var hud = JGProgressHUD(style: .dark)
    var banner: GADBannerView?
    var rewardBasedVideo: GADRewardBasedVideoAd = GADRewardBasedVideoAd.sharedInstance()
    let view_xu = Bundle.main.loadNibNamed("KiemXuHocBaiView", owner: self, options: nil)?.first as! KiemXuHocBaiView
    var searchBar = UISearchBar()
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isConnectedToNetwork()
        self.showButtonLeft(img: "ic_nav_back")
        self.showButtonRight(img: "ic_menu_black_24dp")
        if self.checkExpiredOffAds() { // hiện quảng cáo
            self.checkPoint()
            DispatchQueue.main.async {
                self.loadAdsXu()
            }
        }
    }
    
    func setBackButton() {
        self.setLeftNavigationButton("ic_nav_back")
    }
    
    func showSearchBar(searchBar: UISearchBar){
        searchBar.sizeToFit()
        searchBar.placeholder = "Tìm kiếm..."
        searchBar.delegate = self
        searchBar.barTintColor = .red
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.white
            textfield.textColor = .black
            textfield.tintColor = .black
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.navigationColor])

            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.lightGray
            }

        }
        self.navigationItem.titleView = searchBar
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.checkExpiredOffAds() { // hiện quảng cáo
            self.view_xu.txtXu.text = "\(Defaults[.point]!) XU"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                               object: nil)
    }
    
    @objc func handlePurchaseNotification(_ notification: Notification) { // mua thành công thì + 30 ngày
        guard let productID = notification.object as? String else { return }
        if RageProducts.identifierConsumable == productID {
            print(productID)
            // chưa đăng ký gói nào hoặc đã đăng ký nhưng hết hạn
            if Defaults[.endDate] == nil || self.checkExpiredOffAds() {
                Defaults[.startDate] = Date().current()
                Defaults[.endDate] = Date().addMonth(n: 2)
            }else { // đang có 1 gói chưa hết hạn
                Defaults[.endDate] = Defaults[.endDate]!.addMonth(n: 2)
            }
            Defaults[.enable] = true
            self.openViewControllerBasedOnIdentifier("HomeViewController")
        }else {
            self.showAlerClose("Thông báo", productID, closeHandler: nil)
        }
    }
    
    func checkDateExpiredInAppPurchase() { // kiểm tra xem đã hết hạn gói hay chưa
        if let endDate = Defaults[.endDate] {
            let endDateStr = endDate.toString()
            let currentDate = Date().current().toString()
            print("current date -> ", currentDate)
            print("end date -> ",     endDate)
            let resultCompare = Date.format(endDateStr, currentDate)
            if resultCompare == .Early || resultCompare == .Equal {
                appDelegate?.scheduleNotification(message: "Gia hạn gói để không hiển thị quảng cáo, làm trắc nghiệm và xem video !")
            }
            
            if Defaults[.enable] != nil, resultCompare == .Equal {
                
                self.showAlerYesNo(title: "Thông báo", "Gia hạn", "Không gia hạn", "Gói sắp hết hạn, bạn có muốn gia hạn tiếp không ?", yesHandler: { (alert) in
                    // tiếp tục gia hạn
                    RageProducts.store.buyProductWithProductIdentifier(RageProducts.identifierConsumable)
                }) { (alertno) in
                    // không gia hạn nữa
                    //                    Defaults[.startDate]    = nil
                    //                    Defaults[.endDate]      = nil
                    Defaults[.enable]       = nil
                }
            }else if Defaults[.enable] != nil, resultCompare == .Early {
                self.showAlerYesNo(title: "Thông báo", "Gia hạn", "Không gia hạn", "Gói đã hết hạn, bạn có muốn gia hạn tiếp không ?", yesHandler: { (alert) in
                    // tiếp tục gia hạn
                    RageProducts.store.buyProductWithProductIdentifier(RageProducts.identifierConsumable)
                }) { (alertno) in
                    // không gia hạn nữa
                    //                    Defaults[.startDate]    = nil
                    //                    Defaults[.endDate]      = nil
                    Defaults[.enable]       = nil
                }
            }
        }
    }
    
    func checkExpiredOffAds() -> Bool {
        if let endDate = Defaults[.endDate] {
            let endDateStr = endDate.toString()
            let currentDate = Date().current().toString()
            print("current date -> ", currentDate)
            print("end date -> ",     endDate)
            let resultCompare = Date.format(endDateStr, currentDate)
            if resultCompare == .Later || resultCompare == .Equal { // chưa hết hạn
                return false // khoong hiện quảng cáo
            }
        }
        return true   // hiện quảng cáo
    }
    
    func checkPoint() {
        if Defaults[.point] == nil {
            Defaults[.point] = 10
        }
    }
    
    func showButtonLeft(img: String) {
        self.navigationItem.leftBarButtonItem = nil
        let leftButton = UIBarButtonItem(image: UIImage(named: img), style: .plain, target: self, action: #selector(self.handleLeftBar))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
    }
    
    func showButtonRight(img: String) {
        self.navigationItem.rightBarButtonItems = nil
        let leftButton = UIBarButtonItem(image: UIImage(named: img), style: .plain, target: self, action: #selector(onSlideMenuButtonPressed(_:)))
        self.navigationItem.rightBarButtonItem = leftButton
    }
    
    @objc func handleLeftBar(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func removeAds () {
        self.banner = nil
    }
    
    func showAds(_ view: UIView){
        banner = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(banner!, view)
        banner!.adUnitID = Constant.ads_banner // id con that
        banner!.rootViewController = self
        banner!.load(GADRequest())
        banner!.delegate = self
        self.banner!.alpha = 0
        
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView, _ view_ads: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        bannerView.frame = CGRect(x: 0, y: view_ads.frame.height - 35, width: 320, height: 50)
        view_ads.addSubview(bannerView)
        view_ads.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: view_ads,
                                attribute: .bottom,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view_ads,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
        view_xu.handleDelegate = self
        view_xu.frame = CGRect(x: 0, y: 0 , width: view_ads.frame.width, height: 33)
        view_xu.translatesAutoresizingMaskIntoConstraints = true
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
        view_ads.addSubview(view_xu)
    }
    
    func handleViewADS() {
        if rewardBasedVideo.isReady {
            rewardBasedVideo.present(fromRootViewController: self)
        }else {
            self.loadAdsXu()
            print("-----------------Chua load dc ads-----------------")
        }
    }
    
    // banner setup
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        self.banner?.alpha = 1
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
    }
    // banner end setup
    
    // MARK: GADRewardBasedVideoAdDelegate implementation
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load: \(error.localizedDescription)")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is closed.")
        if rewardBasedVideo.isReady == false {
            self.loadAdsXu()
        }
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
        Defaults[.point]! += 5
        self.showAlertActionSheet("Bạn đã được cộng 10 xu để học bài!")
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func loadAdsXu() {
        rewardBasedVideo.delegate = self
        rewardBasedVideo.load(GADRequest(), withAdUnitID: Constant.ads_add_xu)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        switch(index){
        case 0:
            self.openViewControllerBasedOnIdentifier("HomeViewController")
            break
        case 1:
            //            faceBookShare()
            let textToShare = String(describing: "Chia sẻ ứng dụng của bạn \(Constant.url_app)")
            // URL, and or, and image to share with other apps
            guard let myAppURLToShare = URL(string: Constant.url_app), let image = UIImage(named: "LOGO60") else {
                return
            }
            let items = [textToShare, myAppURLToShare, image] as [Any]
            let avc = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            //Apps to exclude sharing to
            avc.excludedActivityTypes = [
                .airDrop, .print, .saveToCameraRoll, .addToReadingList, .mail, .message,
                .postToFacebook, .postToFlickr, .postToTwitter, .postToTencentWeibo
            ]
            //If user on iPad
            if UIDevice.current.userInterfaceIdiom == .pad {
                if avc.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    avc.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                }
            }
            //Present the shareView on iPhone
            self.present(avc, animated: true, completion: nil)
        case 2:
            let url_fb = Constant.app_facebook
            let appURL = Constant.app_facebook
            self.openURL(url_fb, appURL)
            break
        case 3:
            let url_fb = Constant.url_youtube
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
            break
            
        case 4:
            RageProducts.store.buyProductWithProductIdentifier(RageProducts.identifierConsumable)
        default:
            print("default\n", terminator: "")
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        if let wd = UIApplication.shared.delegate?.window {
            wd!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        }
        
        //        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        //
        //        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        //        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
        //            print("Same VC")
        //        } else {
        //            self.navigationController!.pushViewController(destViewController, animated: true)
        //        }
    }
    
    func faceBookShare(){
        let content = FBSDKShareLinkContent()
        content.contentURL =  URL(string: Constant.url_app)
        let dialog : FBSDKShareDialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = FBSDKShareDialogMode.automatic
        dialog.show()
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = 1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        //        navigationController?.setNavigationBarHidden(true, animated: true)
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC = MenuViewController()
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
    
    func animateView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
    func showAlertActionSheet(_ str: String ) {
        let importantAlert: UIAlertController = UIAlertController(title: str, message:"", preferredStyle: .actionSheet)
        if ( UI_USER_INTERFACE_IDIOM() == .pad ){
            if let popoverController = importantAlert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
                self.present(importantAlert, animated: true, completion: nil)
            }
        }else{
            self.present(importantAlert, animated: true, completion: nil)
        }
        
        let delay = 1.5 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            importantAlert.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func showAlerYesNo(title: String,_ tileYes: String, _ titleNo: String, _ message: String?, yesHandler handlerYes: ((UIAlertAction) -> Void)?, noHandler handlerNo: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: tileYes, style: .default, handler: handlerYes)
        let closeAction = UIAlertAction(title: titleNo, style: .default, handler: handlerNo)
        alert.addAction(closeAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlerClose(_ title: String, _ message: String?, closeHandler handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Đóng", style: .cancel, handler: handler)
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isConnectedToNetwork() {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            self.showAlerClose("Thông báo", "Mạng không ổn định, vui lòng kiểm tra lại kết nối của bạn !", closeHandler: nil)
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        print("isReachable: ----> ", isReachable)
        print("needsConnection: ----> ", needsConnection)
        if !(isReachable && !needsConnection) {
            self.showAlerClose("Thông báo", "Mạng không ổn định, vui lòng kiểm tra lại kết nối của bạn !", closeHandler: nil)
            return
        }
    }
}

extension BaseViewController { /* Check nil value
     *************************************************/
    func openURL(_ url: String, _ app: String){
        guard let url = URL(string: url) else {
            return
        }
        if app != "", let app = URL(string: app) {
            if UIApplication.shared.canOpenURL(app) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(app, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(app)
                }
            }else {
                //redirect to safari because the user doesn't have Instagram
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else {
            //redirect to safari because the user doesn't have Instagram
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func nilString(_ value: String?) -> String{
        if let v = value {
            return v
        }
        return ""
    }
    
    func nilNumber(_ value: NSNumber?) -> NSNumber{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilInt(_ value: Int?) -> Int{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilFloat(_ value: Float?) -> Float{
        if let n = value {
            return n
        }
        return 0
    }
}

extension UIViewController {
    func present(_ owner: UIViewController, completion: (() -> Void)? = nil) {
        owner.present(self, animated: true, completion: completion)
    }

    func presentFullScreen(_ vc: UIViewController, completion: (() -> Void)? = nil) {
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: completion)
    }

    func pushViewWithScreen(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func setLeftNavigationButton(_ imageName: String) {
        self.navigationItem.leftBarButtonItems = nil
        self.navigationItem.hidesBackButton = true
        let button = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: #selector(actionLeftBar))
        self.navigationItem.leftBarButtonItem = button
    }

    func setRightNavigationButton(_ imageName: String) {
        self.navigationItem.rightBarButtonItems = nil
        let button = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: #selector(actionRightBar))
        self.navigationItem.rightBarButtonItem = button
    }

    @objc func actionLeftBar() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func actionRightBar() { }
}
