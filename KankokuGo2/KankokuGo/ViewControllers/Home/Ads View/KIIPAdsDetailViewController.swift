//
//  KIIPAdsDetailViewController.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 28/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class KIIPAdsDetailViewController: BaseViewController {
    @IBOutlet weak var containerWebView: ContainerWebView!
    @IBOutlet weak var googleAdsView: GoogleAdsView! {
        didSet {
            self.googleAdsView.isHidden = false
            self.googleAdsView.loadAds(self, type: .Banner)
        }
    }

    var txtTitle: String?
    var link = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        let lb = UILabel()
        navigationItem.titleView = lb
        lb.frame = navigationItem.titleView?.bounds ??  CGRect.init(x: 0, y: 0, width: 200, height: 20)
        lb.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        lb.textColor = .white
        lb.text = self.txtTitle
        self.setBackButton()
        self.containerWebView.webView.scrollView.delegate = self
        self.containerWebView.loadHTML(htmlContent: self.getHtmlString(from: self.link))
    }

    func getHtmlString(from string: String) -> String {
        let link = string.replacingOccurrences(of: "\r\n", with: "<br />", options: NSString.CompareOptions.literal, range:nil).replacingOccurrences(of: "\n", with: "<br />", options: NSString.CompareOptions.literal, range:nil)
        
        let headerString = "<header><meta name='viewport' content='width=\(self.containerWebView.webView.frame.size.width), initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0, user-scalable=yes'></header>"
        let modifiedText = String(format: "<span>%@</span>", link)
        let htmlString = "\(headerString)\(modifiedText)"
        return htmlString
    }
}

extension KIIPAdsDetailViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
