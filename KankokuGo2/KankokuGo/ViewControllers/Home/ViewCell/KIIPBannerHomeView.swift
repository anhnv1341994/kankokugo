//
//  KIIPBannerHomeView.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 13/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher
import FirebaseDatabase

class KIIPBannerHomeView: CustomViewXib {
    @IBOutlet weak var pageView: FSPagerView!
    @IBOutlet weak var pageControl: UIPageControl!
    private var pageSelected: Int = 0 {
        didSet {
            self.pageControl.currentPage = self.pageSelected
        }
    }
    var didSelectItemAt: ((_ id: String?) -> Void)?
    var photos: [BannerResponse] = []

    override func setupUI() {
        self.defauSetupPagerView()
        self.getListBanner()
    }

    func getListBanner() {
        let ref = Database.database().reference()
        ref.child("quangCao").observe(.value) { snapshot in
            if let array = snapshot.value as? [[String: Any]] {
                var list: [BannerResponse] = []

                for aValue in array {
                    print(aValue)
                    let id = aValue["id"] as? String
                    let linkAnh = aValue["linkAnh"] as? String
                    let banner = BannerResponse(id: id, linkAnh: linkAnh, data: nil)
                    list.append(banner)
                }

                self.photos = list
                self.setupPageControl()
                self.pageView.reloadData()
            }
        }
    }

    func defauSetupPagerView() {
        self.pageView.delegate = self
        self.pageView.dataSource = self
        self.pageView.register(UINib(nibName: "KIIPHomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")

        self.pageView.scrollDirection = .horizontal
        self.pageView.itemSize = UIApplication.shared.windows.first!.frame.size
        self.pageView.interitemSpacing = 0
        self.pageView.transformer = FSPagerViewTransformer(type: .linear)
        self.pageView.decelerationDistance = FSPagerView.automaticDistance
        self.pageView.automaticSlidingInterval = 3.0
        self.pageView.isInfinite = true
    }

    private func setupPageControl() {
        self.pageControl.numberOfPages = self.photos.count
        self.pageControl.currentPage = self.pageSelected
        self.pageControl.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
    }
}

extension KIIPBannerHomeView: FSPagerViewDataSource, FSPagerViewDelegate {
    // MARK:- FSPagerView DataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.photos.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! KIIPHomeCollectionViewCell
        let photo = self.photos[index]
        cell.imgView.kf.setImage(with: URL(string: photo.linkAnh ?? ""))
        return cell
    }

    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.didSelectItemAt?(self.photos[index].id)
    }

    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageSelected = targetIndex
    }

    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageSelected = pageView.currentIndex
    }
}

struct BannerResponse: Codable {
    let id: String?
    let linkAnh: String?
    let data: [BannerInfo]?
}

struct BannerInfo: Codable {
    let post_content: String?
    var post_title: String?
}
