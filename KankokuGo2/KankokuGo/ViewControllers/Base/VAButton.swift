//
//  VAButton.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 17/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class VAButton: UIButton {
    @IBInspectable var cornerRadius1: CGFloat = 0.0 {
        didSet {
            if self.cornerRadius != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var borderColor1: UIColor? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var borderColorType: UIColor? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var selectedBorderColor: UIColor? {
        let color: UIColor?
        if let colorType = self.borderColorType {
            color = colorType
        }
        else if let selectedColor = self.borderColor {
            color = selectedColor
        }
        else {
            color = nil
        }
        return color
    }
    
    @IBInspectable var borderWidth1: CGFloat = 0.0 {
        didSet {
            if self.borderWidth != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var topLeft: Bool = false {
        didSet {
            if self.topLeft != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var topRight: Bool = false {
        didSet {
            if self.topRight != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var bottomLeft: Bool = false {
        didSet {
            if self.bottomLeft != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var bottomRight: Bool = false {
        didSet {
            if self.bottomRight != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    private var lastBounds: CGSize = CGSize.zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    func initialize() -> Void {
        self.isOpaque = false
        self.clipsToBounds = true
        self.lastBounds = self.bounds.size
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let _ = self.superview {
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.lastBounds.equalTo(self.bounds.size) == false {
            self.lastBounds = self.bounds.size
            
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        var corners: UIRectCorner = []
        
        if self.topLeft {
            corners = [corners, .topLeft]
        }
        
        if self.topRight {
            corners = [corners, .topRight]
        }
        
        if self.bottomLeft {
            corners = [corners, .bottomLeft]
        }
        
        if self.bottomRight {
            corners = [corners, .bottomRight]
        }
        
        let color = (self.backgroundColor ?? .white)
        
        let roundedRect = self.bounds.insetBy(dx: self.borderWidth / 2.0, dy: self.borderWidth / 2.0)
        let cornerRadii = CGSize(width: self.cornerRadius, height: self.cornerRadius)
        let bezierPath = UIBezierPath(roundedRect: roundedRect,
                                      byRoundingCorners: corners,
                                      cornerRadii: cornerRadii)
        color.setFill()
        bezierPath.fill()
        
        if let color = self.selectedBorderColor {
            bezierPath.lineWidth = self.borderWidth
            color.setStroke()
            bezierPath.stroke()
        }
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = bezierPath.cgPath
        self.layer.mask = maskLayer
    }
}
