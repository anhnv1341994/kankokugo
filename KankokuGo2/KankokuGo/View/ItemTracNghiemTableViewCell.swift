//
//  ItemTracNghiemTableViewCell.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 05/16/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit

protocol CheckAnswer: class {
    func check(indexRow: Int, answer: Int)
}

class ItemTracNghiemTableViewCell: UITableViewCell {
    @IBOutlet weak var txtNumberQuestion: UILabel!
    @IBOutlet weak var txtQuestion: UILabel!
    @IBOutlet weak var txtAnswer: UILabel!
    @IBOutlet weak var txtLabelAnswer: UILabel!
    @IBOutlet weak var txtAnsweA: UILabel!
    @IBOutlet weak var txtAnsweB: UILabel!
    @IBOutlet weak var txtAnsweC: UILabel!
    @IBOutlet weak var txtAnsweD: UILabel!
    @IBOutlet weak var imgAnsweA: UIImageView!
    @IBOutlet weak var imgAnsweB: UIImageView!
    @IBOutlet weak var imgAnsweC: UIImageView!
    @IBOutlet weak var imgAnsweD: UIImageView!
    @IBOutlet weak var viewAnsweA: UIView!
    @IBOutlet weak var viewAnsweB: UIView!
    @IBOutlet weak var viewAnsweC: UIView!
    @IBOutlet weak var viewAnsweD: UIView!
    @IBOutlet weak var viewAnswer: UIView!
    @IBOutlet weak var marginLeftLabel: NSLayoutConstraint!
    var delegate: CheckAnswer?
    var index: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapA = UITapGestureRecognizer(target: self, action: #selector(self.actionTapAnswer(_ :)))
        let tapB = UITapGestureRecognizer(target: self, action: #selector(self.actionTapAnswer(_ :)))
        let tapC = UITapGestureRecognizer(target: self, action: #selector(self.actionTapAnswer(_ :)))
        let tapD = UITapGestureRecognizer(target: self, action: #selector(self.actionTapAnswer(_ :)))
        viewAnsweA.addGestureRecognizer(tapA)
        viewAnsweB.addGestureRecognizer(tapB)
        viewAnsweC.addGestureRecognizer(tapC)
        viewAnsweD.addGestureRecognizer(tapD)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func actionTapAnswer(_ sender: UITapGestureRecognizer) {
        let tag = sender.view!.tag
        if self.delegate != nil {
            self.delegate?.check(indexRow: self.index, answer: tag)
        }
    }
}
