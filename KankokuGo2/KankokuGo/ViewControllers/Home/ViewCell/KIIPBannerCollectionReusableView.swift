//
//  KIIPBannerCollectionReusableView.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 13/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class KIIPBannerCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var bannerView: KIIPBannerHomeView!
    var didSelectItemAt: ((_ id: String?) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.bannerView.didSelectItemAt = self.didSelectItemAt
    }
}
