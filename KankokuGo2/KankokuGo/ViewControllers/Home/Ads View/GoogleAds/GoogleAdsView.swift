//
//  NotificationView.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/29/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import GoogleMobileAds
import UIKit

enum TypeAds {
    case Banner, Interstitial, video
}

/*
 ID TEST:     <string>ca-app-pub-3940256099942544~1458002511</string>

 ca-app-pub-8910656916911970~3943914631

 ca-app-pub-8910656916911970/5197286550 banner

 ca-app-pub-8910656916911970/4921954386 intiliti
 
 */

class GoogleAdsView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    var interstitial: GADInterstitial?
    var rewardedAd: GADAdReward?
    weak var rootVC: UIViewController?
    var adRequestInProgress = false
    var didRewardFromAds: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    private func setupView() {
//        if rewardedAd?.isReady == false || rewardedAd == nil {
//            rewardedAd = createAndLoadRewardedAd()
//        }

        NotificationCenter.default.removeObserver(self)
//        NotificationCenter.default.addObserver(self, selector: #selector(hideAndShow), name: .showHideAds, object: nil)
        Bundle.main.loadNibNamed("GoogleAdsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    func loadAds(_ rootVC: UIViewController, type: TypeAds, completion: ((Bool) -> Void)? = nil) {
        print("Google Mobile Ads SDK version: \(GADRequest.version())")
        self.rootVC = rootVC
        switch type {
        case .Banner:
            loadBanner(rootVC)
//        case .video:
//            self.loadVideo(rootVC, completion: completion)
        default:
            loadInterstitial(rootVC)
        }
    }

    @objc func hideAndShow() {
//        print("->>>>>>>>>>>>>>>>>>>>>>>>>>>>> hideAndShow \(self.className)")
//        self.isHidden = UserDefaults.checkValidateMonth
    }

    deinit {
//        NotificationCenter.default.removeObserver(self, name: .showHideAds, object: nil)
//        print("->>>>>>>>>>>>>>>>>>>>>>>>>>>>> Deinit \(self.className)")
    }
}

//extension GoogleAdsView: GADRewardedAdDelegate {
//    private func loadVideo(_ rootVC: UIViewController, completion: ((Bool) -> Void)? = nil) {
//        if rewardedAd?.isReady == true {
//            completion?(true)
//            rewardedAd?.present(fromRootViewController: rootVC, delegate: self)
//        }else {
//            completion?(false)
//        }
//    }
//
//    private func createAndLoadRewardedAd() -> GADRewardedAd? {
//        rewardedAd = GADRewardedAd(adUnitID: AppConstants.adwardID)
//        rewardedAd?.load(GADRequest()) { error in
//            if let error = error {
//                print("Loading failed: \(error)")
//            } else {
//                print("Loading Succeeded")
//            }
//        }
//        return rewardedAd
//    }
//
//    /// Tells the delegate that the user earned a reward.
//    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
//        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
//        self.didRewardFromAds?()
//    }
//    /// Tells the delegate that the rewarded ad was presented.
//    func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
//        print("Rewarded ad presented.")
//    }
//    /// Tells the delegate that the rewarded ad was dismissed.
//    func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
//        self.rewardedAd = createAndLoadRewardedAd()
//        print("Rewarded ad dismissed.")
//    }
//    /// Tells the delegate that the rewarded ad failed to present.
//    func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
//        print("Rewarded ad failed to present.")
//    }
//}

extension GoogleAdsView: GADBannerViewDelegate {
    private func loadBanner(_ rootVC: UIViewController) {
        bannerView.adUnitID = Constant.ads_banner
        bannerView.rootViewController = rootVC
        bannerView.load(GADRequest())
    }

    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}

// MARK: - GADInterstitial

extension GoogleAdsView: GADInterstitialDelegate {
    fileprivate func loadInterstitial(_ rootVC: UIViewController) {
        interstitial = createAndLoadInterstitial()
    }

    private func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: Constant.ads_full)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }

    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if ad.isReady {
            ad.present(fromRootViewController: rootVC!)
        } else {
            print("Ad wasn't ready")
        }
    }

    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }

    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }

    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
//        interstitial = createAndLoadInterstitial()
    }

    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}
