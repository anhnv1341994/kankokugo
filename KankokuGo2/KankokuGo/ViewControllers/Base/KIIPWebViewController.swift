//
//  KIIPWebViewController.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 19/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import WebKit

class KIIPWebViewController: BaseViewController {
    @IBOutlet weak var containerWebView: ContainerWebView!

    var txtTitle: String?
    var link = ""
    var isNotSubPoint = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.txtTitle
        self.setBackButton()
        self.containerWebView.webView.scrollView.delegate = self
        self.containerWebView.loadHTML(htmlContent: self.getHtmlString(from: self.link))
    }
    
    func getHtmlString(from string: String) -> String {
        let link = string.replacingOccurrences(of: "\r\n", with: "<br />", options: NSString.CompareOptions.literal, range:nil).replacingOccurrences(of: "\n", with: "<br />", options: NSString.CompareOptions.literal, range:nil)
        
        let headerString = "<header><meta name='viewport' content='width=\(self.containerWebView.webView.frame.size.width), initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0, user-scalable=yes'></header>"
        let modifiedText = String(format: "<span>%@</span>", link)
        let htmlString = "\(headerString)\(modifiedText)"
        return htmlString
    }
}

extension KIIPWebViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
