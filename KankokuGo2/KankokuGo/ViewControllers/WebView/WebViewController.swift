//
//  WebViewController.swift
//  AKSwiftSlideMenu
//
//  Created by tld on 4/2/19.
//  Copyright © 2019 Kode. All rights reserved.
//

import UIKit
import WebKit
import SwiftyUserDefaults
import GoogleMobileAds

class WebViewController: BaseViewController {
    var interstitial: GADInterstitial!
    @IBOutlet weak var viewAds: UIView!
    @IBOutlet weak var height_ads: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    var txtTitle: String = ""
    var link = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.showButtonRight(img: "ic_menu_black_24dp")
        self.navigationItem.title = self.txtTitle
        configWebView()
        if self.checkExpiredOffAds() {
            if let point_ads = Defaults[.point_ads] {
                if point_ads < 2 { // nếu chưa đủ 3 lần học thì cho xem tiếp mà k hiện quảng cáo
                    Defaults[.point_ads]! += 1
                    DispatchQueue.main.async {
                        self.loadWebView()
                    }
                    self.showAlertActionSheet("Bạn đã bị trừ 1 xu để học bài")
                }else {
                    Defaults[.point_ads] = nil // nếu 3 lần xem bài học thì hiện ads full
                    DispatchQueue.main.async {
                        self.createAndLoadInterstitial()
                    }
                }
            }else {
                Defaults[.point_ads] = 1
                DispatchQueue.main.async {
                    self.loadWebView()
                }
                self.showAlertActionSheet("Bạn đã bị trừ 1 xu để học bài")
            }
            Defaults[.point]! -= 1
            self.view_xu.txtXu.text = "\(Defaults[.point]!) XU"
        }else {
            DispatchQueue.main.async {
                self.loadWebView()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.checkExpiredOffAds() {
            if Defaults[.point_ads] != 3 {
                self.height_ads.constant = Constant.height_ads
                DispatchQueue.main.async {
                    self.showAds(self.viewAds)
                }
            }
        }else {
            self.height_ads.constant = 0
            self.removeAds()
        }
//        self.animateView()
    }
    
    // MARK: - Config webview
    
    func configWebView() {
        webView.scrollView.bounces = false
        if #available(iOS 11.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        self.view.addSubview(webView)
    }
    
    func loadWebView() {
        let link = self.link.replacingOccurrences(of: "\r\n", with: "<br />", options: NSString.CompareOptions.literal, range:nil).replacingOccurrences(of: "\n", with: "<br />", options: NSString.CompareOptions.literal, range:nil)
        print("content --->. ", link)
        loadWebView(webView: self.webView, link: link)
    }
    
    func loadWebView(webView: WKWebView, link: String) {
        webView.loadHTMLString(link, baseURL: nil)
        
        //        if let url = URL(string: link) {
        //            let request = URLRequest(url: url)
        //            webView.loadHTMLString(link, baseURL: nil)
        //        }
    }
    
}

extension WebViewController: UIAlertViewDelegate, GADInterstitialDelegate {
    fileprivate func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: Constant.ads_full) // id con that
        interstitial.delegate = self
        interstitial.load(DFPRequest())
        DispatchQueue.main.async {
            self.loadWebView()
        }
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        self.height_ads.constant = Constant.height_ads
        DispatchQueue.main.async {
            self.loadWebView()
        }
        DispatchQueue.main.async {
            self.showAds(self.viewAds)
        }
        self.showAlertActionSheet("Bạn đã bị trừ 1 xu để học bài")
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.showAlertActionSheet("Bạn đã bị trừ 1 xu để học bài")
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        self.height_ads.constant = Constant.height_ads
        DispatchQueue.main.async {
            self.showAds(self.viewAds)
        }
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
    }
}


// MARK: - WebViewDelegate

extension WebViewController: WKUIDelegate, WKNavigationDelegate {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url), let url = self.webView.url, UIApplication.shared.canOpenURL(url) {
            print("### URL:", url)
            
        }
        
        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            // When page load finishes. Should work on each page reload.
            if self.webView.estimatedProgress == 1 {
                print("### EP:", self.webView.estimatedProgress)
                
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
        if let link = webView.url?.absoluteString {
            print("Did commit url: \(link)")
        } else {
            print("Did commit")
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print("start provisional")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("fail to load")
        
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("fail provision")
        
    }
    
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print("Process terminate")
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let protectSpace = challenge.protectionSpace
        let sender = challenge.sender
        
        if protectSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let truct = challenge.protectionSpace.serverTrust {
                let credential = URLCredential(trust: truct)
                let Device = UIDevice.current
                let iosVersion = NSString(string: Device.systemVersion).doubleValue
                if iosVersion >= 9.0 && iosVersion < 10.0 {
                    sender?.use(credential, for: challenge)
                }
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
            }
            
        } else {
            let credential = URLCredential(user: "", password: "", persistence: .forSession)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
        }
    }
}

extension WebViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
