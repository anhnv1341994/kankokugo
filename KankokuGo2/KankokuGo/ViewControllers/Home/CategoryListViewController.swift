//
//  CategoryListViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/24/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class CategoryListViewController:  BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewAds: UIView!
    @IBOutlet weak var height_ads: NSLayoutConstraint!
    var slug: String = ""
    var nav_title: String = ""
    var list_title: [DataPosts] = []
    var term_id: NSNumber? // id cua thu muc sau khi get ve
    var page = 1
    var last_page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.nav_title.uppercased()
        self.showSearchBar(searchBar: self.searchBar)
        self.tableView.register(UINib(nibName: "TitleViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        DispatchQueue.main.async {
            self.getDataTerm("\(self.slug)")
        }
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.checkExpiredOffAds() {
            self.height_ads.constant = Constant.height_ads
            DispatchQueue.main.async {
                self.showAds(self.viewAds)
            }
        }else {
            self.height_ads.constant = 0
            self.removeAds()
        }
//        self.animateView()
    }
}

extension CategoryListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list_title.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TitleViewCell {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
            cell.backgroundColor = UIColor.clear
            cell.txtTitle.text = self.nilString(self.list_title[indexPath.item].post_title)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.slug.components(separatedBy: "tracnghiem").count > 1 {
//            if self.checkExpiredOffAds() { //hien thi quang cao
//                let alert = self.instantiateViewController(ofType: AlertRegisterPayViewController.self)
//                alert.modalTransitionStyle = .crossDissolve
//                alert.modalPresentationStyle = .overFullScreen
//                self.present(alert, animated: true, completion: nil)
//            }else { // da mua goi khong quang cao
                let tracnghiem = self.instantiateViewController(fromStoryboard: .main, ofType: TracNghiemViewController.self)
                tracnghiem.content = list_title[indexPath.row].post_content ?? ""
                self.navigationController?.pushViewController(tracnghiem, animated: true)
//            }
        }else {
            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: WebViewController.self)
            webVC.txtTitle = self.nilString(list_title[indexPath.row].post_title)
            webVC.link = list_title[indexPath.row].post_content ?? ""
            if self.checkExpiredOffAds() { //hien thi quang cao
                if Defaults[.point]! > 0 {
                    self.navigationController?.pushViewController(webVC, animated: true)
                }else {
                    self.showAlertActionSheet("Bạn vui lòng kiếm xu để học bài")
                }
            }else {
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height;
        let contentYoffset = scrollView.contentOffset.y;
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        if (distanceFromBottom < height + 1), contentYoffset != 0, self.last_page > self.page {
            self.page += 1
            self.getData(self.nilNumber(self.term_id), self.page)
            self.tableView.reloadData()
        }
    }
}

extension CategoryListViewController {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.page = 1
            self.searchData(self.nilNumber(self.term_id), title: "")
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.page = 1
        self.searchData(self.nilNumber(self.term_id), title: searchBar.text!)
    }
}

extension CategoryListViewController {
    func getData(_ term_id: NSNumber, _ page: Int) {
        self.hud.show(in: self.view)
        NetworkManager.sharedManager.get_data_term_post("\(term_id)?page=\(page)") { (result) in
            self.hud.dismiss(animated: true)
            if (result.result.isFailure) {
                //                self.showAlerClose(MNConstant.thongbao, "Đang bảo trì hệ thống xin hãy quay lại sau.", closeHandler: nil)
            }else {
                var check: Bool = true
                if let value = result.result.value {
                    if let code = value.code, code == 200 {
                        check = false
                        if let data_obj = value.data, let datas = data_obj.posts?.data {
                            for data in datas {
                                self.list_title.append(data)
                            }
                            self.last_page = self.nilNumber(data_obj.posts?.last_page).intValue
                        }
                        self.tableView.reloadData()
                    }
                    if check {
                        var message = ""
                        if let msg = value.message {
                            message = msg
                        }else if let msg = value.data?.message{
                            message = msg
                        }
                    }
                }
            }
        }
    }
    
    func getDataTerm(_ str: String) {
        self.hud.show(in: self.view)
        NetworkManager.sharedManager.get_data_term(str) { (result) in
            self.hud.dismiss(animated: true)
            if (result.result.isFailure) {
                
            }else {
                var check: Bool = true
                if let value = result.result.value {
                    if let code = value.code, code == 200 {
                        check = false
                        if let data_obj = value.data {
                            if let terms = data_obj.terms, let data = terms.data, data.count > 0, let term_id = data[0].term_id {
                                self.term_id = term_id
                                DispatchQueue.main.async {
                                    self.getData(term_id, 1)
                                }
                            }
                        }
                    }
                    if check {
                        var message = ""
                        if let msg = value.message {
                            message = msg
                        }else if let msg = value.data?.message{
                            message = msg
                        }
                    }
                }
            }
        }
    }
    
    func searchData (_ term_id: NSNumber, title: String) {
        self.list_title = []
        self.tableView.reloadData()
        self.hud.show(in: self.view)
        NetworkManager.sharedManager.searchData(term_id, title: title) { (result) in
            self.hud.dismiss(animated: true)
            if (result.result.isFailure) {
                //                self.showAlerClose(MNConstant.thongbao, "Đang bảo trì hệ thống xin hãy quay lại sau.", closeHandler: nil)
            }else {
                var check: Bool = true
                if let value = result.result.value {
                    if let code = value.code, code == 200 {
                        check = false
                        if let data_obj = value.data, let datas = data_obj.posts?.data {
                            for data in datas {
                                self.list_title.append(data)
                            }
                        }
                        self.tableView.reloadData()
                    }
                    if check {
                        var message = ""
                        if let msg = value.message {
                            message = msg
                        }else if let msg = value.data?.message{
                            message = msg
                        }
                    }
                }
            }
        }
    }
}


