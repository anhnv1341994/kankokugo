//
//  KIIPADSViewController.swift
//  KIIP2021
//
//  Created by Nguyen Anh on 28/03/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class KIIPADSViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var googleAdsView: GoogleAdsView! {
        didSet {
            self.googleAdsView.isHidden = false
            self.googleAdsView.loadAds(self, type: .Banner)
        }
    }
    var datas: [AdsData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MUA SẮM"
        self.setupCollectionView()
        self.setBackButton()
        self.collectionView.contentOffset = CGPoint(x: self.collectionView.contentOffset.x, y: 0)
        self.getDataAdsID()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }

    private func setupCollectionView() {
        if let flowLayout = self.collectionView.collectionViewLayout as? GridCollectionViewLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionInset = UIEdgeInsets.init(top: 10.0, left: 10.0, bottom: 2.0, right: 10.0)
            flowLayout.interitemSpacing = 10.0
            flowLayout.lineSpacing = 10.0
            flowLayout.aspectRatio = 2 / 2.2

            if UI_USER_INTERFACE_IDIOM() == .pad {
                flowLayout.numberOfItemsPerLine = 4
            }
            else {
                flowLayout.numberOfItemsPerLine = 3
            }
        }

        self.collectionView.register(cellType: KIIPAdsCollectionViewCell.self)
    }

    func getDataAdsID() {
        self.hud.show(in: self.view)
        NetworkManager.sharedManager.getDataAdsID { (response, error) in
            if let data = response?.data?.terms?.data, data.count > 0, let id = data.first?.term_id {
                self.getDataAdsList(id, pageNumber: "1")
                self.title = data.first?.name?.uppercased()
            } else {
                self.hud.dismiss(animated: true)
                self.showAlerClose("", error?.localizedDescription, closeHandler: nil)
            }
        }
    }

    private func getDataAdsList(_ id: String, pageNumber: String) {
        NetworkManager.sharedManager.getDataAdsList(id, pageNumber: pageNumber) { (response, error) in
            self.hud.dismiss(animated: true)
            if let datas = response?.data, datas.count > 0 {
                self.datas = datas
                self.collectionView.reloadData()
            } else {
                self.showAlerClose("", error?.localizedDescription, closeHandler: nil)
            }
        }
    }
}

extension KIIPADSViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datas.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: KIIPAdsCollectionViewCell.self, for: indexPath)
        let item = self.datas[indexPath.item]
        if let url = item.embedded.wpFeaturedmedia.first?.mediaDetails.sizes["thumbnail"]?.sourceURL {
            cell.imgView.kf.setImage(with: URL(string: url))
        }else {
            cell.imgView.kf.setImage(with: URL(string: item.embedded.wpFeaturedmedia.first?.sourceURL ?? ""))
        }
        cell.lbMonth.text = item.title.rendered
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.datas[indexPath.item]
        let webVC = KIIPAdsDetailViewController()
        webVC.txtTitle = item.title.rendered
        webVC.link = item.content.rendered
        self.pushViewWithScreen(webVC)
    }
}
